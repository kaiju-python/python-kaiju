"""Contains a python safe user code executor classes and functions."""

import asyncio
import datetime
import re
import textwrap
import uuid
from hashlib import md5
from concurrent.futures import ThreadPoolExecutor
from concurrent.futures import TimeoutError as ConcurrentTimeoutError
from collections import deque
from inspect import isfunction, isbuiltin
from types import SimpleNamespace
from typing import *

from kaiju.abc import Hashable, Serializable
from kaiju.helpers import async_run_in_thread

__all__ = ['CodeExecutor']


class CodeExecutor(Serializable):
    """A code executor capable of potentially safe user source code execution
    and termination.

    Examples:
    ---------

    Compile and run any allowed user code.

    >>> import asyncio
    >>> code = "return 7 * 6"
    >>> executor = CodeExecutor(code)
    >>> asyncio.get_event_loop().run_until_complete(executor.run())
    42

    List all available user functions and constants.

    >>> _ = CodeExecutor.functions()

    Register a new function in a class (be careful and read the method readme
    first).

    >>> def some_function(x, y):
    ...     return x * y
    >>> CodeExecutor.register(some_function)

    """

    MAX_EXECUTION_TIME = 100    #: maximum execution time in ms
    MAX_CODE_SIZE = 1000        #: maximum function size in symbols

    not_allowed = {
        'del ', 'yield', 'global', 'nonlocal', 'globals', 'locals',
        'def ', 'class', 'lambda', 'raise', '__', 'try', 'import'
    }  #: these keywords are not allowed in code functions

    allowed_functions = {
        'abs', 'all', 'any', 'bool', 'bytes', 'dict', 'divmod', 'enumerate',
        'filter', 'float', 'format', 'frozenset', 'hex', 'int', 'iter', 'len',
        'list', 'map', 'max', 'min', 'next', 'oct', 'ord', 'pow', 'range',
        'repr', 'reversed', 'round', 'set', 'sorted', 'str', 'sum', 'tuple',
        'zip'
    }  #: list of builtin commands allowed in code functions

    allowed_modules = {
        'base64', 'math', 'random', 're', 'string', 'textwrap', 'uuid'
    }  #: list of modules allowed in code functions

    _code_function = 'code_function'

    # regex code string validators -------------------------------------------

    _keyword_regex = '|'.join(not_allowed)
    _keyword_regex = re.compile(f'({_keyword_regex})')

    # constructing a function namespace --------------------------------------

    _functions = {}

    for name in allowed_functions:
        _functions[name] = __builtins__[name]

    _simple_types = (
        int, str, float, bool, bytes, datetime.datetime,
        datetime.timedelta, datetime.time, datetime.date,
        uuid.UUID
    )

    for module in allowed_modules:
        _module = __import__(module)
        _module_functions = {}
        for name, _obj in _module.__dict__.items():
            if not name.startswith('_'):
                if any((
                    isbuiltin(_obj),
                    isfunction(_obj),
                    isinstance(
                        _obj,
                        _simple_types
                    )
                )):
                    _module_functions[name] = _obj

        _functions[module] = _module_functions

    _function_tuple = tuple(_functions.items())
    _user_functions = []

    __slots__ = ('max_exec_time', 'code')

    def __init__(
            self, code: str, id=None, max_exec_time_ms: int = MAX_EXECUTION_TIME,
            format=True, **meta):
        """
        :param id: not used but required for repr() compatibility
        :param code: python code should be prepared
        :param max_exec_time_ms: max script execution time in ms
        :param format: format code before execution
        :param meta: some script metadata
        """

        self.code = self.format(code) if format else code
        self.max_exec_time = max(1, int(max_exec_time_ms)) / 1000
        self.meta_fields = meta

    async def run(self, **kws):
        """Executes a code function.

        :param kws: parameters to be available for the executable code
        :return: a function result if possible

        :raises RuntimeError: if code function execution fails
        :raises TimeoutError: if code function reaches its time limits
        """

        self._validate_variable(kws)

        _locals = {}
        _globals = dict(kws)
        _globals['__builtins__'] = self.builtins()

        # function construction in isolation
        # here it won't raise an error because validation has been already made

        exec(self.code, _globals, _locals)

        # must run in a thread for safety against infinite loops

        try:
            result = await async_run_in_thread(
                _locals[self._code_function], max_exec_time=self.max_exec_time)
        except ConcurrentTimeoutError:
            for f in self._user_functions:
                f.__dict__ = {}
            raise TimeoutError(
                'Code execution exceeded the limit of %s s.'
                % self.max_exec_time)
        except Exception as err:
            for f in self._user_functions:
                f.__dict__ = {}
            raise RuntimeError(
                'An error occurred during a function execution: %s.' % err)
        else:
            for f in self._user_functions:
                f.__dict__ = {}
        return result

    @classmethod
    def format(cls, code: str) -> str:
        """Prepares code to be stored in the database and executed later.

        :param code: function code (without the "def" statement!)
        :returns: formatted code ready for execution
        :raises ValueError: if something is wrong with the code
        """

        if len(code) > cls.MAX_CODE_SIZE:
            raise ValueError(
                'Code block exceeds maximum allowed code block size'
                ' of %s symbols.' % cls.MAX_CODE_SIZE)

        if 'return ' not in code:
            raise ValueError('Code function must return something.')

        key = next(cls._keyword_regex.finditer(code), None)
        if key:
            raise ValueError(
                'Keyword %s is not allowed in a code functions.' % key)

        code.strip(' \t\r\n')
        code = textwrap.dedent(code)
        code = textwrap.indent(code, '\t')
        code = f"def {cls._code_function}():\n{code}\n"

        try:
            exec(code, {}, {})
        except Exception as err:
            # exec raises an exception only if the code is malformed
            # thus it's better to check for bad code on the code string
            # validation
            raise ValueError(
                'An error occurred during a function "%s" evaluation. [%s]: %s.'
                % (code, err.__class__.__name__, err))

        return code

    @classmethod
    def builtins(cls) -> dict:
        """Lists all builtins allowed inside a user code block."""

        d = {}
        for k, v in cls._function_tuple:
            if isinstance(v, dict):
                d[k] = SimpleNamespace(**v)
            else:
                d[k] = v
        return d

    @classmethod
    def functions(cls) -> List[str]:
        """Lists all functions available to a user."""

        _functions = []

        for key, value in cls._function_tuple:
            if isinstance(value, dict):
                for method in value:
                    _functions.append(f'{key}.{method}')
            else:
                _functions.append(key)

        _functions.sort()

        return _functions

    @classmethod
    def register(cls, function):
        """You may register a function in a list of available functions to
        an executor.

        >>> def custom_function(x, y):
        ...     return x * y
        >>> import asyncio
        >>> CodeExecutor.register(custom_function)
        >>> executor = CodeExecutor('return custom_function(x, y)').run(x=7, y=6)
        >>> asyncio.get_event_loop().run_until_complete(executor)
        42

        .. warning::

            Use this method with caution, because user still can destructively
            change a class if you directly pass it to the user, or one can
            use your function to destructively change other code.

            *Example (bad):*

                You register a new class Counter.

                    > from collections import Counter
                    > CodeExecutor.register(Counter)

                User executes a code which utilizes a Counter class itself.

                    > code = 'Counter.items = lambda x: [42];return True'
                    > await CodeExecutor(code).run()

                Now you have a modified Counter method.

                    > Counter({'a': 1, 'b': 2}).items()
                    [42]

            For the reason above you can't pass a class or a module to this
            function. The best way to provide a class to the user would be
            to create a custom class constructor returning an instance of
            the class, because the user won't be able to reach `__class__` or
            `type` methods anyway.

            *Example (good):*

                > from collections import Counter
                > def counter(iterable):
                >     return Counter(iterable)

            Don't register or proxy any builtin methods. They are banned for a
            reason.

            *Example (bad)*:

                > def get_type(obj):
                >     return type(obj)

            The code above will allow user to reach a class object itself and
            change its methods.

        """

        if not isfunction(function):
            raise ValueError('Only functions are allowed.')

        if isbuiltin(function):
            raise ValueError('Builtin functions are not allowed.')

        if function.__name__ in cls._functions:
            raise KeyError(
                'Name %s is already reserved by another function.'
                % function.__name__)

        cls._functions[function.__name__] = function
        cls._function_tuple = list(cls._functions.items())
        cls._function_tuple.sort()
        cls._function_tuple = tuple(cls._function_tuple)
        cls._user_functions.append(function)

    def __hash__(self):
        return self.code.__hash__()

    @property
    def uuid(self):
        _hash = md5(self.code.encode()).hexdigest()
        return uuid.UUID(_hash)

    def repr(self) -> dict:
        """A JSON compatible object representation."""

        return {
            **self.meta_fields,
            'id': self.uuid,
            'code': self.code,
            'max_exec_time_ms': self.max_exec_time,
            'format': False
        }

    @classmethod
    def _validate_variable(cls, variable):
        if not isinstance(variable, cls._simple_types):
            if isinstance(variable, dict):
                deque(map(cls._validate_variable, variable.values()), maxlen=0)
            elif isinstance(variable, (Collection, Iterable)):
                deque(map(cls._validate_variable, variable), maxlen=0)
            elif isinstance(variable, SimpleNamespace):
                deque(map(cls._validate_variable, variable.__dict__.values()), maxlen=0)
            else:
                error = ValueError(
                    'Only simple types and collections of them are allowed'
                    ' to be a function params. If you need a function,'
                    ' register it using the `CodeExecutor.register` method.')
                error.value = variable
                raise error
