import abc
import inspect
import logging
import uuid
from hashlib import md5
from typing import *

from rapidjson import *

from kaiju.json import decoder, encoder

__all__ = [
    'Contextable', 'Loggable', 'Hashable', 'Serializable', 'AbstractClassManager',
    'AbstractClassManagerMeta'
]


class Loggable(abc.ABC):
    """Allows a class to have its own logger.
    It can be accessed from `self.logger` attribute.
    """

    def __init__(self, *args, logger: logging.Logger = None, **kws):
        _logger_name = self.__class__.__name__
        self._logger = logger
        if logger:
            self.logger = logger.getChild(_logger_name)
        else:
            self.logger = logging.getLogger(_logger_name)


class Contextable(abc.ABC):
    """A contextable means that you can use this class in an `async with`
    initialization context."""

    async def init(self):
        """Initialization."""

    async def close(self):
        """Deinitialization."""

    @property
    def closed(self) -> bool:
        """You need to provide a condition for a closed state."""

        return False

    async def __aenter__(self):
        await self.init()
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        await self.close()


class Hashable(abc.ABC):
    """A hashable object that can be used as a key and in set collections
    and can be serialized / deserialized."""

    @abc.abstractmethod
    def __hash__(self):
        """Must return an unique object hash based on its attributes."""

    def __eq__(self, other):
        return (self.__hash__() == other.__hash__()) and type(self) is type(other)

    @property
    def uuid(self) -> uuid.UUID:
        """An UUID hash-based object identifier."""

        return uuid.UUID(int=self.__hash__())


class Serializable(Hashable, abc.ABC):
    """This means that an object can be serialized / deserialized to and from
    JSON or a repr string."""

    def __iter__(self):
        return iter(self.repr())

    def __getitem__(self, item):
        return self.repr()[item]

    def __len__(self):
        return len(self.repr())

    def keys(self):
        return self.repr().keys()

    @abc.abstractmethod
    def repr(self) -> dict:
        """Must return a representation of object __init__ arguments."""

    def __hash__(self):
        d = dumps(
            self.repr(), uuid_mode=UM_CANONICAL, ensure_ascii=True,
            datetime_mode=DM_ISO8601, number_mode=NM_DECIMAL, allow_nan=False,
            default=dict, sort_keys=True)
        return int(md5(d.encode()).hexdigest(), 16)

    def json(self) -> str:
        return encoder(self.repr())

    @classmethod
    def from_json(cls, item: str):
        return cls(**decoder(item))

    def __repr__(self):
        return f'{self.__class__.__name__}(**{self.repr()})'

    def __str__(self):
        return f'<{self.__class__.__name__} instance>'


class AbstractClassManagerMeta(abc.ABCMeta):

    def __init__(cls, *args, **kws):
        super().__init__(*args, **kws)
        if abc.ABC not in cls.__bases__:
            cls._validate_class_manager(cls)

    @staticmethod
    def _validate_class_manager(cls):
        if not cls._class:
            raise ValueError(f'%s._class attribute must be set.' % cls.__qualname__)
        cls._classes = {}

    def __contains__(cls, item: Union[str, Type]):
        if inspect.isclass(item):
            item = cls.class_key(item)
        return item in cls._classes

    def __getitem__(cls, item) -> Type:
        return cls._classes[item]
    #
    # def __getattr__(cls, item) -> Type:
    #     return cls[item]

    def __iter__(cls):
        return iter(cls._classes.items())

    def __len__(cls):
        return len(cls._classes)

    def keys(cls):
        return cls._classes.keys()

    def get(cls, key, default=None):
        return cls._classes.get(key, default)


class AbstractClassManager(abc.ABC, metaclass=AbstractClassManagerMeta):
    """Class manager with the ability to register other classes.

    Usage:

    >>> class Base:
    ...     pass
    >>> class Custom(Base):
    ...     x = 1
    >>> class YourManager(AbstractClassManager):
    ...     _class = Base

    Registering classes:

    >>> YourManager.register_class(Custom)

    Checking for availability and accessing classes by their names:

    >>> 'Custom' in YourManager
    True
    >>> YourManager.Custom is YourManager['Custom']
    True
    >>> YourManager.get('Unknown', False)
    False
    >>> dict(YourManager)
    {'Custom': <class 'abc.Custom'>}

    """

    _class = None    #: here you MUST specify your class base type
    _classes = None

    def __contains__(self, item: Union[str, Type]):
        return AbstractClassManagerMeta.__contains__(self.__class__, item)

    def __getitem__(self, item: str) -> Type:
        return AbstractClassManagerMeta.__getitem__(self.__class__, item)

    def __getattr__(self, item: str) -> Type:
        return AbstractClassManagerMeta.__getattr__(self.__class__, item)

    def __iter__(self):
        return AbstractClassManagerMeta.__iter__(self.__class__)

    @classmethod
    def can_register(cls, obj: Type) -> bool:
        """Checks if object can be registered."""

        return inspect.isclass(obj) and not inspect.isabstract(obj) and issubclass(obj, cls._class)

    @staticmethod
    def class_key(obj) -> str:
        """Defines the name by which a registered class will be referenced."""

        if inspect.isclass(obj):
            return obj.__name__
        else:
            return obj.__class__.__name__

    @classmethod
    def register_class(cls, obj: Type):
        """Registers a new service class in the service context manager.

        :raises TypeError: if the class can't be registered for some reason
        """

        if cls.can_register(obj):
            cls._register_class(obj)
        else:
            raise TypeError(
                f'Class "{obj}" cannot be registered'
                f' because it\'s an abstract class'
                f' or it is not a subclass of base class'
                f' "{cls._class.__name__}".')

    @classmethod
    def register_classes_from_globals(cls):
        """Registers multiple classes from globals() of the module."""

        return cls._register_classes_from_namespace(globals())

    @classmethod
    def register_classes_from_module(cls, module):
        """Registers multiple classes from a module. Classes not inherited from
        cls._class will be ignored."""

        return cls._register_classes_from_namespace(module.__dict__)

    @classmethod
    def _register_class(cls, obj: Type):
        cls._classes[cls.class_key(obj)] = obj

    @classmethod
    def _register_classes_from_namespace(cls, namespace: dict):
        for obj in namespace.values():
            if cls.can_register(obj):
                cls._register_class(obj)
