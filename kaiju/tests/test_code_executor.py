import asyncio
import time

import pytest

from kaiju.code_executor import CodeExecutor


@pytest.mark.benchmark
async def test_code_executor_performance(performance_test):

    async def _test(num):

        code = """
        s = math.cos({n})
        return s
        """

        codes = [
            CodeExecutor.format(code.format(n=n))
            for n in range(num)
        ]

        t0 = time.time()

        for code in codes:
            await CodeExecutor(code, format=False).run()

        t1 = time.time()

        return t1 - t0, num

    num, runs = 3000, 3
    print(f'\n\nTesting code executor performance ({num} cycles, {runs} runs)...\n')
    dt, counter, rps = performance_test(_test, args=(num,), runs=runs)
    print(f'\t{rps:>10} sec^-1 {dt}')

    print()


@pytest.mark.unit
async def test_code_executor(logger):

    logger.info('Testing normal code execution.')

    code = """
    z = f'{x}{y}'
    return str(z)
    """

    x, y = 4, 2
    executor = CodeExecutor(code)
    result = await executor.run(x=x, y=y)
    assert result == '42'

    logger.info('Testing module imports.')

    code = """
    s = math.cos(0)
    return str(int(s))
    """

    executor = CodeExecutor(code)
    result = await executor.run()
    assert result == '1'

    logger.info('Testing multiple executor calls at once.')

    code = """
    y = math.pow(x, 2)
    return y
    """

    values = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    expected = [1, 4, 9, 16, 25, 36, 49, 64,  81, 100]
    executor = CodeExecutor(code)
    results = await asyncio.gather(*(
        executor.run(x=value)
        for value in values
    ))
    assert results == expected

    logger.info('Testing non existing variables.')

    code = """
    return x
    """

    with pytest.raises(RuntimeError):
        await CodeExecutor(code).run()

    logger.info('Testing module attribute assignment errors.')

    import math

    code = """
    math.sin = 42
    return math.sin
    """

    await CodeExecutor(code).run()
    assert math.sin != 42

    code = """
    return math.sin(0)
    """

    result = await CodeExecutor(code).run()
    assert result == 0

    logger.info('Testing code execution with special symbols.')

    code = "x=0\nreturn math.sin(x)"

    result = await CodeExecutor(code).run()
    assert result == 0

    logger.info('Testing function attributes assignment cleanup.')

    def custom_func(x):
        return x

    CodeExecutor.register(custom_func)

    code = """
    custom_func.attr = 1
    return 1
    """

    await CodeExecutor(code).run()
    assert 'attr' not in custom_func.__dict__
    assert 'attr' not in custom_func.__class__.__dict__

    logger.info('Testing indentation errors.')

    code = """
    'wrong'
        'indents'
      'found'
      return 42
    """

    with pytest.raises(ValueError):
        CodeExecutor(code)

    logger.info('Testing no returns errors.')

    code = """
    "no returns"
    return
    """

    with pytest.raises(ValueError):
        CodeExecutor(code)

    logger.info('Testing for non allowed keywords.')

    code = """
    del x
    return 1 
    """

    with pytest.raises(ValueError):
        await CodeExecutor(code).run(x=42)

    logger.info('Testing code execution errors.')

    code = """
    x = 2 + 'a'
    return x
    """

    with pytest.raises(RuntimeError):
        await CodeExecutor(code).run()

    logger.info('Testing code termination on timeout.')

    code = """
        c = 0
        while True:
            c += 1
        return 42
    """

    with pytest.raises(TimeoutError):
        await CodeExecutor(code, max_exec_time_ms=10).run()

    logger.info('Testing potentially unsafe variables.')

    code = """
        sht('_' + '_import_' + '_("os")')
        os.removedirs('/hereisyourroot')
        return "don't worry, dicker, you'll never see this..."
    """

    with pytest.raises(ValueError):
        await CodeExecutor(code).run(sht=exec)

    logger.info('Testing unsafe python code evaluation.')

    code = "(" * 256 + ")" * 256

    code = f"""
    {code}
    return 42
    """

    with pytest.raises(ValueError):
        await CodeExecutor(code).run()

    logger.info('Testing hashing.')

    code = """
    return sum(range(100))
    """

    cache = set()
    exec_1 = CodeExecutor(code)
    cache.add(exec_1)
    exec_2 = CodeExecutor(code)
    assert hash(exec_1) == hash(exec_2)
    assert exec_2 in cache

    logger.info('Testing string serialization.')

    code = """
    return x + y
    """

    executor = CodeExecutor(code, max_exec_time_ms=5)
    from uuid import UUID
    executor = eval(repr(executor))
    result = await executor.run(x=21, y=21)
    assert result == 42

    logger.info('Testing UUID function.')

    executor = CodeExecutor(code)
    executor.uuid

    logger.info('finished tests')
