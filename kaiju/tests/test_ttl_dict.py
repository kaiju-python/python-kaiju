from collections import deque
from time import time

import pytest

from kaiju.ttl_dict import TTLDict


@pytest.mark.benchmark
def test_ttl_dict_performance(performance_test):

    def _test_get(num, size):
        ttl = TTLDict(zip(range(size), range(size)))
        t0 = time()
        deque((ttl[n % size] for n in range(num)), maxlen=0)
        t1 = time()
        return t1 - t0, num

    def _test_set(num, size):
        ttl = TTLDict(zip(range(size), range(size)))
        t0 = time()
        deque((ttl.__setitem__(n % size, n) for n in range(num)), maxlen=0)
        t1 = time()
        return t1 - t0, num

    def _test_len(num, size):
        ttl = TTLDict(zip(range(size), range(size)))
        t0 = time()
        deque((ttl.__len__() for _ in range(num)), maxlen=0)
        t1 = time()
        return t1 - t0, num

    num, runs = 3000, 3
    sizes = [10, 100, 1000]

    print(f'\n\nTesting TTL dict performance ({num} cycles, {runs} runs)...\n')

    print(f'\tGET:')
    for size in sizes:
        dt, counter, rps = performance_test(_test_get, args=(num, size), runs=runs)
        print(f'\t\t{size:>4} {rps:>10} sec^-1 {dt}')

    print(f'\tSET:')
    for size in sizes:
        dt, counter, rps = performance_test(_test_set, args=(num, size), runs=runs)
        print(f'\t\t{size:>4} {rps:>10} sec^-1 {dt}')

    print(f'\tLENGTH:')
    for size in sizes:
        dt, counter, rps = performance_test(_test_len, args=(num, size), runs=runs)
        print(f'\t\t{size:>4} {rps:>10} sec^-1 {dt}')

    print()


@pytest.mark.unit
async def test_ttl_dict(logger):

    fixture = {'a': 42}

    logger.info('Testing basic operation.')

    ttl = TTLDict(**fixture)
    assert dict(ttl) == fixture
    assert list(ttl.items()) == list(fixture.items())
    assert list(ttl.values()) == list(fixture.values())
    assert dict(**ttl) == fixture
    assert ttl.pop('a') == fixture['a']
    assert len(ttl) < len(fixture)
    assert not ttl
    ttl['b'] = 42
    assert 'b' in ttl

    logger.info('Testing ttl operations.')

    with pytest.raises(ValueError):
        ttl.set_ttl(0)

    ttl.set_ttl(0.000000001)

    assert not ttl
    assert ttl.get('b') is None

    logger.info('Finished tests.')
