import enum
import uuid
from datetime import datetime
from typing import *

import croniter

from kaiju.abc import Serializable
from kaiju.rpc.jsonrpc import *

__all__ = ['Task', 'TaskCommand', 'TaskCommandSet', 'State']


class State(enum.Enum):
    IDLE = 'idle'
    AWAITING = 'awaiting'
    QUEUED = 'queued'
    EXEC = 'executed'
    ABORTED = 'aborted'
    FINISHED = 'finished'


class TaskCommand(Serializable):
    """Task command specification."""

    class TemplateKeys(enum.Enum):
        COMMAND = 'cmd'
        HOOK = 'hook'
        RESULT = 'result'
        PREV_COMMAND = 'prev_cmd'
        PREV_HOOK = 'prev_hook'
        PREV_RESULT = 'prev_result'

    __slots__ = (
        'id', 'request', 'result_from_previous_command',
        'on_failure_hook', 'call_hook_if_task_set_fails', 'pass_result_to_hook'
    )

    PROCEED_ON_ERRORS = False

    def __init__(
            self, cmd: dict,
            on_failure_hook: dict = None,
            proceed_on_errors: bool = PROCEED_ON_ERRORS
    ):
        """
        :param cmd: RPC request command
        :param proceed_on_errors: if True, then the command sequence will proceed
            even if the command have failed
        :param on_failure_hook: method to execute on failure, (the command will
            be considered failed anyway)
        """

        cmd = RPCRequest(method=cmd['method'], params=cmd.get('params'))
        self.cmd = {
            'method': cmd.method,
            'params': cmd.params
        }
        if on_failure_hook:
            on_failure_hook = RPCRequest(
                method=on_failure_hook['method'],
                params=on_failure_hook.get('params'))
            on_failure_hook = {
                'method': on_failure_hook.method,
                'params': on_failure_hook.params
            }
        self.on_failure_hook = on_failure_hook
        self.proceed_on_errors = bool(proceed_on_errors)

    def repr(self):
        return {
            'cmd': self.cmd,
            'on_failure_hook': self.on_failure_hook,
            'proceed_on_errors': self.proceed_on_errors
        }


class TaskCommandSet(Serializable):

    __slots__ = ('commands',)

    def __init__(self, commands):

        self.commands = []

        def _process_command(c):
            if isinstance(c, TaskCommand):
                self.commands.append(c)
            else:
                self.commands.append(TaskCommand(**c))

        if isinstance(commands, list):
            for c in commands:
                _process_command(c)
        else:
            _process_command(commands)

    def __iter__(self):
        return iter(self.commands)

    def repr(self):
        return {
            'commands': [
                cmd.repr() for cmd in self.commands
            ]
        }


class Task(Serializable):
    """A task object."""

    def __init__(
            self,
            cmd: Union[dict, list, TaskCommandSet],
            name: str = None,
            cron: str = None,
            state: Union[str, State] = State.IDLE,
            active: bool = True,

            created: datetime = None,
            state_change: datetime = None,
            last_run: datetime = None,
            next_run: datetime = None,
            start_from: datetime = None,
            exec_deadline: datetime = None,

            user_id: uuid.UUID = None,
            job_id: uuid.UUID = None,
            worker_id: uuid.UUID = None,

            notify: bool = True,
            exit_code: int = None,
            result: dict = None,

            id=None):

        if not isinstance(cmd, TaskCommandSet):
            cmd = TaskCommandSet(**cmd)

        self.cmd = cmd
        self.active = active

        self.name = name if name else str(self.uuid)

        if type(state) is str:
            state = State(state)
        self.state = state

        if created is None:
            created = datetime.now()

        self.created = created

        if state_change is None:
            state_change = created

        self.state_change = state_change
        self.last_run = last_run
        self.start_from = start_from if start_from else state_change

        self.cron = cron
        if not cron:
            next_run = self.start_from
        self.next_run = next_run
        self.exec_deadline = exec_deadline

        self.user_id = user_id
        self.job_id = job_id
        self.worker_id = worker_id

        self.exit_code = exit_code
        self.result = result
        self.notify = notify

    @property
    def id(self):
        return self.cmd.uuid

    def __hash__(self):
        return self.cmd.__hash__()

    def change_state(self, new_state: State):
        self.state = new_state
        self.state_change = datetime.now()

    def iter(self):
        if self.cron:
            cron = croniter.croniter(self.cron, start_time=self.state_change)
            cron_iter = cron.all_next(datetime)
            t = datetime.now()
            next_run = self.next_run
            while not next_run or next_run < t:
                next_run = next(cron_iter)
            self.next_run = next_run
            self.exec_deadline = next(cron_iter)

    def repr(self) -> dict:
        return {

            'id': self.uuid,
            'name': self.name,
            'cmd': self.cmd,
            'state': self.state.value,
            'active': self.active,
            'cron': self.cron,

            'created': self.created,
            'state_change': self.state_change,
            'last_run': self.last_run,
            'next_run': self.next_run,
            'start_from': self.start_from,
            'exec_deadline': self.exec_deadline,

            'user_id': self.user_id,
            'job_id': self.job_id,
            'worker_id': self.worker_id,

            'notify': self.notify,
            'exit_code': self.exit_code,
            'result': self.result

        }
