import pytest

__all__ = [
    'ldap_test_settings'
]


@pytest.fixture(scope='session')
def ldap_test_settings() -> dict:
    """LDAP service settings for localhost.

    You may run your OpenLDAP server for the tests using Docker:

        docker run -p 389:389 -p 636:636 --name ldap LDAP_ADMIN_PASSWORD="admin" osixia/openldap:latest

    """

    return {
        'host': 'ldap://127.0.0.1',
        'user': 'cn=admin,dc=example,dc=org',
        'password': 'admin'
    }
