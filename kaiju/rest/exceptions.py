"""Набор шаблонов классов REST исключений.
Данные исключения перехватываются REST API обработчиком исключений, для них
генерится JSON строка ответа. Остальные исключения будут видимы только в логах
приложения (или на странице, если был включен debug режим).
"""

import traceback
import uuid

from aiohttp.web import middleware, Response, Request
from aiohttp.web_exceptions import *
from aiohttp.client import ClientResponseError

from kaiju.json import encoder
from kaiju.abc import Serializable

__all__ = [
    'RESTApiException', 'ValidationError', 'NotFound', 'ServerUnavailableError',
    'NotAllowed', 'Conflict', 'JSONParseError', 'DataError', 'AccessDeniedError',
    'UnauthorizedError',
    'error_middleware', 'PartiallySucceeded', 'REQUEST_ID_HEADER'
]

REQUEST_ID_HEADER = 'X-Request-ID'


class RESTApiException(Serializable, Exception):
    """Ошибка сервера."""

    __slots__ = ('message', 'extras')

    status_code = 500

    def __init__(self, msg: str, **extras):
        self.message = msg if msg else self.__doc__
        self.extras = extras

    def repr(self) -> dict:
        return {
            'msg': self.message,
            'extras': self.extras
        }

    def __str__(self):
        return self.message


class PartiallySucceeded(RESTApiException):
    """Частично выполненный запрос."""

    status_code = 200


class DataError(RESTApiException):
    """Ошибка сервера при работе с данными."""


class ServerUnavailableError(RESTApiException):
    """Внешний сервис недоступен."""

    status_code = 503

    def __init__(self, msg: str, remote_url: str, **extras):
        super().__init__(msg, remote_url=remote_url, **extras)


class UnauthorizedError(RESTApiException):
    """Доступ к ресурсу запрещен по какой-либо причине."""

    status_code = 401


class AccessDeniedError(RESTApiException):
    """Доступ к ресурсу запрещен по какой-либо причине."""

    status_code = 403


class JSONParseError(RESTApiException, ValueError):
    """Неверно форматированный JSON."""

    status_code = 400

    def __init__(self, msg: str, json: str, **extras):
        super().__init__(msg, json=json, **extras)


class ValidationError(RESTApiException):
    """Ошибка при валидации данных запроса."""

    status_code = 422


class FailedDependency(RESTApiException):
    """Ошибка при валидации данных запроса."""

    status_code = 424

    def __init__(self, msg: str, key: str = "", value: str = "", **extras):
        super().__init__(msg, key=key, value=value, **extras)


class NotFound(RESTApiException):
    """Ошибка при запросе объекта или страницы: ресурс не найден."""

    status_code = 404

    def __init__(self, msg: str, key: str, **extras):
        super().__init__(msg, key=key, **extras)


class Conflict(RESTApiException):
    """Ошибка при добавлении объекта или страницы: конфликт с существующим объектом."""

    status_code = 409

    def __init__(self, msg: str, key: str, **extras):
        super().__init__(msg, key=key, **extras)


class NotAllowed(RESTApiException):
    """HTTP метод не разрешенный при работе с данным ресурсом."""

    status_code = 405

    def __init__(self, msg: str, method: str, **extras):
        super().__init__(msg, method=method, **extras)


def create_rest_exception_object(exc: RESTApiException) -> dict:
    """Процессит объект ошибки и создает из него REST-compatible объект."""

    error = {
        'code': exc.__class__.__name__,
        **exc.repr()
    }

    return {'error': error}


@middleware
async def error_middleware(request: Request, handler):
    """Простая мидлваре для ошибок с поддержкой sentry."""

    h = REQUEST_ID_HEADER

    if h in request.headers:
        request['id'] = request.headers[h]
    else:
        request['id'] = str(uuid.uuid4())

    logger = request.app.logger.getChild(f'request.{request["id"]}')
    debug = request.app.debug
    request['logger'] = logger

    try:
        response = await handler(request)
    except Exception as err:

        if isinstance(err, RESTApiException):
            logger.info(err)
            status = err.status_code
            error = create_rest_exception_object(err)

        elif isinstance(err, HTTPNotFound):
            status = 404
            error = {
                'error': {
                    'message': 'страница не найдена.'
                }
            }
        elif isinstance(err, ClientResponseError):
            error = err.message
            status = err.status

        else:
            status = 500
            trace = '\n'.join(traceback.format_tb(err.__traceback__))
            detailed = {
                'detail': str(err),
                'code': err.__class__.__name__,
                'traceback': trace
            }
            logger.exception(detailed)
            error = {
                'error': {
                    'message': 'Внутренняя ошибка. Бойтесь её!'
                }
            }
            if debug:
                error['error']['extra'] = detailed

        headers = {
            'Content-Type': 'application/json; charset=utf-8',
            h: request['id']
        }
        error = encoder(error)
        return Response(status=status, text=error, headers=headers)

    response.headers[h] = request['id']
    return response
