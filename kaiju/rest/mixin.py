from abc import abstractmethod, ABC


class AbstractClientMixin(ABC):
    @abstractmethod
    async def request(self, method: str, uri: str, *args, data=None,
                      json=None, params=None, headers=None, **kws) -> dict:
        ...
