"""Used for bulk registration of services in the ServiceContextManager."""

import kaiju.auth
import kaiju.CLI
import kaiju.db
import kaiju.es
import kaiju.files
import kaiju.notifications
import kaiju.rpc
import kaiju.tasks
import kaiju.cache
import kaiju.http_client
import kaiju.settings

from kaiju.services import ServiceContextManager

ServiceContextManager.register_classes_from_module(kaiju.auth)
ServiceContextManager.register_classes_from_module(kaiju.CLI)
ServiceContextManager.register_classes_from_module(kaiju.db)
ServiceContextManager.register_classes_from_module(kaiju.es)
ServiceContextManager.register_classes_from_module(kaiju.files)
ServiceContextManager.register_classes_from_module(kaiju.notifications)
ServiceContextManager.register_classes_from_module(kaiju.rpc)
ServiceContextManager.register_classes_from_module(kaiju.tasks)
ServiceContextManager.register_classes_from_module(kaiju.http_client)
ServiceContextManager.register_classes_from_module(kaiju.settings)
ServiceContextManager.register_classes_from_module(kaiju.cache)
