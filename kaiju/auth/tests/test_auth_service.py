import pytest

from kaiju.json import encoder, decoder
from kaiju.rest.exceptions import ValidationError
from .. import AuthorizationService, SessionService


@pytest.mark.unit
async def test_auth_session_service_initialization_and_basic_op(logger):

    async with SessionService(app_key='shite', logger=logger) as sessions:
        logger.info('Testing data encryption')
        data = {'shite': True}
        _data = encoder(data).encode()
        _data = sessions._decrypt_session_data(
            sessions._key, sessions._encrypt_session_data(sessions._key, _data))
        _data = decoder(_data)
        assert _data['shite'] == data['shite']


@pytest.mark.unit
async def test_auth_service_initialization_and_basic_op(logger):

    async with AuthorizationService(logger=logger) as auth:

        logger.info('Testing passwords validation')

        with pytest.raises(ValidationError):
            auth.validate_password('shit')

        with pytest.raises(ValidationError):
            auth.validate_password('otroreyoeryoeryo')

        auth.validate_password('oetE(Rekw32034glb')

        logger.info('Testing username validation')

        with pytest.raises(ValidationError):
            auth.validate_username('s')

        with pytest.raises(ValidationError):
            auth.validate_username('$shitter%')

        auth.validate_username('normal_user-name')

        logger.info('Testing email validation')

        with pytest.raises(ValidationError):
            auth.validate_email('wrong@mail')

        auth.validate_email('test@mail.ru')
