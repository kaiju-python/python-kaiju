"""Spec generator for JSON RPC compatible with OpenAPI 3.0."""

import inspect
import re
from copy import deepcopy

__all__ = [
    'get_method_spec', 'base_spec', 'components', 'base_rpc_tag', 'base_path_spec',
    'APP_ID_HEADER', 'CORRELATION_ID_HEADER', 'SERVER_ID_HEADER',
    'REQUEST_DEADLINE_HEADER', 'CONTENT_TYPE_HEADER', 'REQUEST_TIMEOUT_HEADER'
]

APP_ID_HEADER = 'X-App-ID'
CORRELATION_ID_HEADER = 'X-Correlation-ID'
SERVER_ID_HEADER = 'X-Server-ID'
REQUEST_DEADLINE_HEADER = 'X-Request-Deadline'
REQUEST_TIMEOUT_HEADER = 'X-Request-Timeout'
CONTENT_TYPE_HEADER = 'Content-Type'

base_spec = {
  "openapi": "3.0.1",
  "info": {
    "title": "JSON RPC/HTTP Server",
    "description": "This is an auto-generated documentation for a JSON RPC HTTP compatible server API.",
    "termsOfService": "http://swagger.io/terms/",
    "contact": {
      "email": "taraskin.a@marketapp.io"
    },
    "license": {
      "name": "Apache 2.0",
      "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
    },
    "version": "1.0.0"
  },
  "externalDocs": {
    "description": "About Marketapp project",
    "url": "http://docs.marketapp.io"
  },
  "servers": [
    {
      "url": "https://localhost/"
    }
  ],
  "tags": [],
  "paths": {},
  "components": {}
}

base_rpc_tag = {
  "name": "rpc",
  "description": "JSON RPC HTTP compatible API",
  "externalDocs": {
    "description": "About Marketapp project",
    "url": "http://docs.marketapp.io"
  }
}

base_path_spec = {
     "post": {
         "tags": [
             "rpc"
         ],
         "summary": "A method-agnostic RPC endpoint.",
         "operationId": "rpcCall",
         "parameters": [
             {
                 "$ref": "#/components/parameters/appId"
             },
             {
                 "$ref": "#/components/parameters/correlationId"
             },
             {
                 "$ref": "#/components/parameters/deadline"
             },
             {
                 "$ref": "#/components/parameters/timeout"
             }
         ],
         "requestBody": {
             "$ref": "#/components/requestBodies/RPCRequestBody"
         },
         "responses": {
             "200": {
                 "$ref": "#/components/responses/RPCResponse"
             },
             "5XX": {
                 "description": "Server error."
             }
         }
     }
}


path_spec = {
    "post": {
        "tags": [],
        "summary": "A method-specific RPC endpoint. You can specify a method name in a request body and call a method-agnostic endpoint at any time.",
        "operationId": "rpcMethodCall",
        "parameters": [
            {
                "$ref": "#/components/parameters/appId"
            },
            {
                "$ref": "#/components/parameters/correlationId"
            },
            {
                "$ref": "#/components/parameters/deadline"
            },
            {
                "$ref": "#/components/parameters/timeout"
            }
        ],
        "requestBody": {
            "$ref": "#/components/requestBodies/RPCRequestBodyMethodSpecific"
        },
        "responses": {
            "200": {
                "$ref": "#/components/responses/RPCResponse"
            },
            "5XX": {
                "description": "Server error."
            }
        }
    }
}

components = {
    "parameters": {
        "appId": {
            "in": "header",
            "name": APP_ID_HEADER,
            "description": "Client application ID. Should be unique.",
            "required": False,
            "schema": {
                "$ref": "#/components/schemas/UUID"
            }
        },
        "correlationId": {
            "in": "header",
            "name": CORRELATION_ID_HEADER,
            "description": "Request correlation ID. Should be unique. Doesn't have to be equal to the JSON RPC request \"id\". If no ID was provided, then a random UUID will be generated and sent back to the client in the headers.",
            "required": False,
            "schema": {
                "$ref": "#/components/schemas/UUID"
            }
        },
        "deadline": {
            "in": "header",
            "name": REQUEST_DEADLINE_HEADER,
            "description": "A request execution deadline in seconds (UNIX time). "
                           "It determines when the request becomes obsolete, "
                           "and a server should generate a request timeout "
                           "message if possible and send it back to the client. "
                           "If an invalid or no value was specified, "
                           "then the default deadline will be set."
                           "If both timeout and deadline were specified "
                           "in the request, then the lower deadline "
                           "will be taken.",
            "required": False,
            "schema": {
                "$ref": "#/components/schemas/UNIXTime"
            }
        },
        "timeout": {
            "in": "header",
            "name": REQUEST_TIMEOUT_HEADER,
            "description": "A request execution timeout in seconds. "
                           "If an invalid or no value was specified, "
                           "then the default deadline will be set. "
                           "If both timeout and deadline were specified "
                           "in the request, then the lower deadline "
                           "will be taken.",
            "required": False,
            "schema": {
                "type": "integer",
                "minimum": 1
            }
        }
    },
    "schemas": {
        "UUID": {
            "type": "string",
            "format": "uuid",
            "description": "UUID string",
            "example": "00000000-0000-0000-0000-00000000002a"
        },
        "UNIXTime": {
            "type": "integer",
            "minimum": 1,
            "maximum": 2147483647,
            "description": "Standard UNIX time in seconds"
        },
        "requestID": {
            "type": "integer",
            "minimum": 0,
            "description": "JSON RPC request ID"
        },
        "method": {
            "type": "string",
            "minLength": 2,
            "pattern": "^([A-z0-9]*(?:\\.[A-z0-9_-]+)+)$",
            "description": "RPC full method name"
        },
        "requestParams": {
            "description": "Request parameters. May be of any type, but must match a method requirements (i.e. they are always method specific)."
        },
        "JSONRPCRequestObject": {
            "type": "object",
            "description": "JSON RPC standard request object",
            "required": [
                "id",
                "method",
                "params"
            ],
            "additionalProperties": False,
            "properties": {
                "id": {
                    "$ref": "#/components/schemas/requestID"
                },
                "method": {
                    "$ref": "#/components/schemas/method"
                },
                "params": {
                    "$ref": "#/components/schemas/requestParams"
                }
            }
        },
        "JSONRPCResponseObject": {
            "type": "object",
            "description": "A valid JSON RPC response object.",
            "required": [
                "id",
                "result"
            ],
            "additionalProperties": False,
            "properties": {
                "id": {
                    "$ref": "#/components/schemas/requestID"
                },
                "result": {
                    "description": "RPC method result of any type."
                }
            }
        },
        "JSONRPCErrorObject": {
            "type": "object",
            "description": "A valid JSON RPC response object.",
            "required": [
                "id",
                "error"
            ],
            "additionalProperties": False,
            "properties": {
                "id": {
                    "$ref": "#/components/schemas/requestID"
                },
                "error": {
                    "type": "object",
                    "description": "A JSON RPC error object.",
                    "required": [
                        "id",
                        "error"
                    ],
                    "additionalProperties": False,
                    "properties": {
                        "id": {
                            "$ref": "#/components/schemas/requestID"
                        },
                        "error": {
                            "type": "object",
                            "required": [
                                "code",
                                "message",
                                "data"
                            ],
                            "additionalProperties": False,
                            "properties": {
                                "code": {
                                    "type": "integer",
                                    "description": "RPC Error code. See JSON RPC protocol reference for the list of standard error codes."
                                },
                                "message": {
                                    "type": "string",
                                    "description": "A user-friendly error description."
                                },
                                "data": {
                                    "type": "object",
                                    "additionalProperties": False,
                                    "description": "An inner data object MAY contain some additional traceback information about the problem.",
                                    "properties": {
                                        "traceback": {
                                            "type": "object",
                                            "additionalProperties": False,
                                            "description": "An error data field MAY contain the traceback information about the code execution failure.",
                                            "required": [
                                                "type",
                                                "repr",
                                                "stack"
                                            ],
                                            "properties": {
                                                "type": {
                                                    "type": "string",
                                                    "description": "Error object type in backend."
                                                },
                                                "repr": {
                                                    "type": "string",
                                                    "description": "Error object representation in backend."
                                                },
                                                "stack": {
                                                    "type": "array",
                                                    "description": "Error traceback stack.",
                                                    "items": {
                                                        "type": "object",
                                                        "description": "Error stack trace element.",
                                                        "required": [
                                                            "filename",
                                                            "lineno",
                                                            "line",
                                                            "name"
                                                        ],
                                                        "additionalProperties": False,
                                                        "properties": {
                                                            "filename": {
                                                                "type": "string",
                                                                "description": "A source file path where the exception occurred."
                                                            },
                                                            "lineno": {
                                                                "type": "string",
                                                                "description": "A line number in the file where the exception occurred."
                                                            },
                                                            "line": {
                                                                "type": "string",
                                                                "description": "A line source code where the exception occurred."
                                                            },
                                                            "name": {
                                                                "type": "string",
                                                                "description": "..."
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },
        "JSONRPCResponses": {
            "oneOf": [
                {
                    "$ref": "#/components/schemas/JSONRPCResponseObject"
                },
                {
                    "$ref": "#/components/schemas/JSONRPCErrorObject"
                }
            ]
        }
    },
    "requestBodies": {
        "RPCRequestBody": {
            "description": "A JSON RPC compatible request or a request batch.",
            "required": True,
            "content": {
                "application/json": {
                    "schema": {
                        "$ref": "#/components/schemas/JSONRPCRequestObject"
                    }
                }
            }
        }
    },
    "responses": {
        "RPCResponse": {
            "description": "A JSON RPC response. If a request contains a batch of objects, then the response will contain a batch of response objects in the same order (but some of the responses may be skipped if some of the request objects have \"notify\" status, i.e. \"null\" IDs).",
            "headers": {
                APP_ID_HEADER: {
                    "description": "Client app ID",
                    "schema": {
                        "$ref": "#/components/schemas/UUID"
                    }
                },
                CORRELATION_ID_HEADER: {
                    "description": "Request correlation ID",
                    "schema": {
                        "$ref": "#/components/schemas/UUID"
                    }
                },
                SERVER_ID_HEADER: {
                    "description": "Server node ID",
                    "schema": {
                        "$ref": "#/components/schemas/UUID"
                    }
                }
            },
            "content": {
                "application/json": {
                    "schema": {
                        "$ref": "#/components/schemas/JSONRPCResponseObject"
                    }
                }
            }
        }
    }
}


# must be lowercase
type_map = {
    'int': {
        'type': 'integer'
    },
    'float': {
        'type': 'number'
    },
    'str': {
        'type': 'string'
    },
    'dict': {
        'type': 'object',
        'properties': {}
    },
    'uuid': {
        'type': 'string',
        'format': 'uuid'
    },
    'bool': {
        'type': 'boolean'
    },
    'list': {
        'type': 'array',
        'items': {}
    },
    'tuple': {
        'type': 'array',
        'items': {}
    },
    'set': {
        'type': 'array',
        'uniqueItems': True,
        'items': {}
    },
    'frozenset': {
        'type': 'array',
        'uniqueItems': True,
        'items': {}
    }
}

doc_spec_finder = re.compile(r'(?:\n:)')

param_spec_finder = re.compile(
    r'\n:param(?:\s+'
    r'(?P<type>[A-z_\.]+))*'
    r'\s+'
    r'(?P<name>[A-z_]+)'
    r'\s*:\s*'
    r'(?P<doc>.+)'
    r'\s*'
)

example_spec_finder = re.compile(
    r'\n:(?:example)(?:\s+'
    r'(?P<name>[A-z_\.]+)'
    r')*\s*:\s*'
    r'(?P<value>.+)*'
    r'\s*'
)

return_spec_finder = re.compile(
    r'\n:(?:return|returns)(?:\s+'
    r'(?P<type>[A-z_\.]+)'
    r')*\s*:\s*'
    r'(?P<doc>.+)*'
    r'\s*'
)

error_spec_finder = re.compile(
    r'\n:(?:raise|raises)(?:\s+'
    r'(?P<type>[A-z_\.]+)'
    r')*\s*:\s*'
    r'(?P<doc>.+)*'
    r'\s*'
)

skipped_params = {'cls', 'mcs', 'self', 'return'}


def get_method_spec(method_name: str, method) -> dict:
    """Generates a python function or method swagger spec from its
    python definition.

    Example:

    >>> spec = get_method_spec('spec', get_method_spec)

    """

    def _get_method_arg_spec(method):

        doc_spec = inspect.getdoc(method)
        if not doc_spec:
            doc_spec = ''

        # doc

        doc = doc_spec_finder.split(doc_spec, maxsplit=1)
        if doc:
            doc = doc[0].strip()
        else:
            doc = None

        # parameters

        param_specs = [m.groupdict() for m in param_spec_finder.finditer(doc_spec)]
        params = {}

        for obj in param_specs:
            data_type = obj.get('type')
            _params = deepcopy(type_map.get(data_type, {}))
            doc = obj['doc']
            if doc:
                _params['description'] = doc
            params[obj['name']] = _params

        # returns

        return_specs = next(return_spec_finder.finditer(doc_spec), None)
        if return_specs:
            obj = return_specs.groupdict()
            data_type = obj.get('type')
            returns = deepcopy(type_map.get(data_type, {}))
            doc = obj['doc']
            if doc:
                returns['description'] = doc
        else:
            returns = {}

        # args

        argspec = inspect.getfullargspec(method)

        data_type = argspec.annotations.get('return')
        if data_type:
            name = getattr(data_type, '__name__', getattr(data_type, '_name', None))
            if name:
                name = name.lower()
                if name in type_map:
                    returns.update(type_map[name])

        for annotation, data_type in argspec.annotations.items():
            if annotation not in skipped_params:
                name = getattr(data_type, '__name__', getattr(data_type, '_name', None))
                if name:
                    name = name.lower()
                    if name in type_map:
                        _spec = type_map[name]
                        if annotation in params:
                            params[annotation].update(_spec)
                        else:
                            params[annotation] = deepcopy(_spec)
                    else:
                        if annotation in params:
                            p = params[annotation]
                            if 'description' not in p:
                                p['description'] = name
                        else:
                            params[annotation] = {
                                'description': name
                            }

        for param in argspec.args:
            if param not in params:
                if param not in skipped_params:
                    params[param] = {
                        'description': 'signature unknown'
                    }

        # examples

        examples = [d.groupdict() for d in example_spec_finder.finditer(doc_spec)]

        for example in examples:
            name = example['name']
            if name in params:
                params[name]['example'] = eval(example['value'])

        return doc, params, returns

    spec = {}

    # request

    doc, params, returns = _get_method_arg_spec(method)

    request = deepcopy(components['schemas']['JSONRPCRequestObject'])
    request['properties']['method'] = {
        "type": "string",
        "enum": [method_name]
    }
    request['properties']['params'] = {
        'type': 'object',
        'properties': params
    }
    request_body = deepcopy(components['requestBodies']['RPCRequestBody'])
    request_body['content']['application/json']['schema'] = request

    # response

    response = deepcopy(components['schemas']['JSONRPCResponseObject'])
    response['properties']['result'] = returns
    response_body = deepcopy(components['responses']['RPCResponse'])
    response_body['content']['application/json']['schema'] = response

    # full path spec

    spec = deepcopy(path_spec)
    spec['post']['requestBody'] = request_body
    spec['post']['operationId'] = f'rpc.{method_name}'
    spec['post']['responses']['200'] = response_body

    if doc:
        spec['post']['summary'] = doc
    else:
        spec['post']['summary'] = method_name

    return spec
