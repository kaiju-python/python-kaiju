"""JSON RPC 2.0 specs.

The only difference from the base protocol at the moment is that the "jsonrpc"
version attribute is disabled.

See examples in docstrings for use suggestions.

For more info see:

    <https://www.jsonrpc.org/specification>`_

"""

import abc
import traceback
import uuid
from typing import *

__all__ = [
    'JSONRPC', 'RPCMessage', 'RPCRequest', 'RPCResponse', 'RPCError',
    'RPCInternalError', 'RPCInvalidParams', 'RPCJSONParseError',
    'RPCMethodNotFound', 'RPCInvalidRequest', 'RPCRequestTimeout',
    'RPCServerClosing', 'RPCPermissionDenied'
]

JSONRPC = '2.0'  #: JSON RPC protocol version

# Base message classes


class RPCMessage(abc.ABC):
    """A base RPC message class compatible with JSON serializator."""

    jsonrpc = JSONRPC

    __slots__ = ('id',)

    def __init__(self, id: Union[int, None], **_):
        self.id = id

    def __iter__(self):
        return iter(self.repr().items())

    def __repr__(self):
        return f'{self.__class__.__name__}(**{self.repr()})'

    @abc.abstractmethod
    def repr(self) -> dict:
        pass

    @property
    def uuid(self):
        return uuid.UUID(int=self.id)


class RPCRequest(RPCMessage):
    """A valid JSONRPC request.

    Example:

    >>> data = {'id': 1, 'method': 'do.test', 'params': [1, 2, 3]}
    >>> RPCRequest(**data)
    RPCRequest(**{'id': 1, 'method': 'do.test', 'params': [1, 2, 3]})

    """

    __slots__ = ('id', 'method', 'params')

    def __init__(
            self, id: Union[int, None] = False, method: str = None,
            params: Union[list, dict] = None, **_):
        """
        :param id: request ID (UUID), None for random uuid4 based integer
        :param method: RPC method or function name
        :param params: RPC method args (list for positional args, dict for kws)
        """

        if id is False:
            self.id = uuid.uuid4().int
        else:
            self.id = id
        if method is None:
            raise TypeError()
        self.method = method
        self.params = params

    def repr(self):
        return {
            'id': self.id,
            'method': self.method,
            'params': self.params
        }


class RPCResponse(RPCMessage):
    """A valid JSONRPC response.

    Example:

    >>> data = {'id': 1, 'method': 'do.test', 'params': [1, 2, 3]}
    >>> request = RPCRequest(**data)
    >>> RPCResponse(request.id, result=42)
    RPCResponse(**{'id': 1, 'result': 42})

    """

    __slots__ = ('id', 'result')

    def __init__(self, id: Union[int, None], result: Any, **_):
        """
        :param id: request ID (for correlation)
        :param result: RPC method call result
        """

        self.id = id
        self.result = result

    def repr(self):
        return {
            'id': self.id,
            'result': self.result
        }


class RPCError(RPCMessage, abc.ABC):
    """A JSONRPC error response."""

    code = 0

    __slots__ = ('id', 'message', 'data', 'base_exc', 'debug')

    def __init__(
            self, id: Union[int, None], message: str = '',
            base_exc: Exception = None, debug: bool = False, data: dict = None,
            **_):
        """
        :param id: request ID (for correlation) or None if it cannot be determined
        :param message: error human-readable message
        :param data: additional metadata, which will be stored in `error.data`
        :param base_exc: base python exception object, you can pass it to trace
            the error, the data will be stored in 'error.data["traceback"]'.
        :param debug: debug mode
        """

        self.id = id
        self.message = message
        self.base_exc = base_exc
        self.data = data if data else {}
        self.debug = debug

    def repr(self):

        if self.base_exc:
            self.data['base_exc_message'] = str(self.base_exc)
            self.data['base_exc_type'] = self.base_exc.__class__.__name__
            self.data['base_exc_data'] = getattr(self.base_exc, 'extras', {})
            if self.debug:
                tb = traceback.TracebackException.from_exception(self.base_exc)
                stack = [
                    {
                        'filename': frame.filename,
                        'lineno': frame.lineno,
                        'name': frame.name,
                        'line': frame.line

                    }
                    for frame in tb.stack
                ]
                self.data['traceback'] = stack

        self.data['type'] = self.__class__.__name__

        result = {
            'id': self.id,
            'error': {
                'code': self.code,
                'message': self.message,
                'data': self.data
            }
        }

        return result

    def __str__(self):
        return f'[{self.code}] {self.message}'

# Standard JSON RPC errors


class RPCJSONParseError(RPCError):
    """This error is raised when a server can't decode a request JSON body."""

    code = -32700

    __slots__ = ('id', 'message', 'data', 'debug', 'base_exc')

    def __init__(self, id: Union[int, None], base_exc: Exception = None, debug: bool = False, data: dict = None, **_):
        """
        :param id: request ID (for correlation) or None if it cannot be determined
        :param data: additional metadata, which will be stored in `error.data`
        :param base_exc: base python exception object, you can pass it to trace
            the error, the data will be stored in 'error.data["traceback"]'.
        :param debug: debug mode
        """

        super().__init__(id, 'JSON Parse Error.', base_exc=base_exc, debug=debug, data=data)


class RPCInvalidRequest(RPCError):
    """This error is raised whenever request format is invalid."""

    code = -32600

    __slots__ = ('id', 'message', 'data', 'debug', 'base_exc')

    def __init__(self, id: Union[int, None], base_exc: Exception = None, debug: bool = False, data: dict = None, **_):
        """
        :param id: request ID (for correlation) or None if it cannot be determined
        :param data: additional metadata, which will be stored in `error.data`
        :param base_exc: base python exception object, you can pass it to trace
            the error, the data will be stored in 'error.data["traceback"]'.
        :param debug: debug mode
        """

        super().__init__(id, 'Invalid request.', base_exc=base_exc, debug=debug, data=data)


class RPCMethodNotFound(RPCError):
    """The RPC server doesn't have a registered method with name specified
    in a request."""

    code = -32601

    __slots__ = ('id', 'message', 'data', 'debug', 'base_exc')

    def __init__(self, id: Union[int, None], base_exc: Exception = None, debug: bool = False, data: dict = None, **_):
        """
        :param id: request ID (for correlation) or None if it cannot be determined
        :param data: additional metadata, which will be stored in `error.data`
        :param base_exc: base python exception object, you can pass it to trace
            the error, the data will be stored in 'error.data["traceback"]'.
        :param debug: debug mode
        """

        super().__init__(id, 'Method not found.', base_exc=base_exc, debug=debug, data=data)


class RPCInvalidParams(RPCError):
    """The RPC server method can't proceed with the provided request params."""

    code = -32602

    __slots__ = ('id', 'message', 'data', 'debug', 'base_exc')

    def __init__(self, id: Union[int, None], base_exc: Exception = None, debug: bool = False, data: dict = None, **_):
        """
        :param id: request ID (for correlation) or None if it cannot be determined
        :param data: additional metadata, which will be stored in `error.data`
        :param base_exc: base python exception object, you can pass it to trace
            the error, the data will be stored in 'error.data["traceback"]'.
        :param debug: debug mode
        """

        super().__init__(id, 'Invalid request params.', base_exc=base_exc, debug=debug, data=data)


class RPCInternalError(RPCError):
    """An internal RPC method error has occured."""

    code = -32603

    __slots__ = ('id', 'message', 'data', 'debug', 'base_exc')

    def __init__(self, id: Union[int, None], base_exc: Exception = None, debug: bool = False, data: dict = None, **_):
        """
        :param id: request ID (for correlation) or None if it cannot be determined
        :param data: additional metadata, which will be stored in `error.data`
        :param base_exc: base python exception object, you can pass it to trace
            the error, the data will be stored in 'error.data["traceback"]'.
        :param debug: debug mode
        """

        super().__init__(id, 'Internal server error.', base_exc=base_exc, debug=debug, data=data)


class RPCRequestTimeout(RPCError):
    """This error is raised whenever request execution deadline is reached."""

    code = 408  #: The timeout error code is not specified in the original JSON RPC spec.
                # so I'm using a similar HTTP codes in these cases

    __slots__ = ('id', 'message', 'data', 'debug', 'base_exc')

    def __init__(self, id: Union[int, None], base_exc: Exception = None, debug: bool = False, data: dict = None, **_):
        """
        :param id: request ID (for correlation) or None if it cannot be determined
        :param data: additional metadata, which will be stored in `error.data`
        :param base_exc: base python exception object, you can pass it to trace
            the error, the data will be stored in 'error.data["traceback"]'.
        :param debug: debug mode
        """

        super().__init__(id, 'Request execution deadline.', base_exc=base_exc, debug=debug, data=data)


class RPCServerClosing(RPCError):
    """This error indicates that the RPC server is stopping and won't handle new requests."""

    code = 503  #: The timeout error code is not specified in the original JSON RPC spec.
                # so I'm using a similar HTTP codes in these cases

    __slots__ = ('id', 'message', 'data', 'debug', 'base_exc')

    def __init__(self, id: Union[int, None], base_exc: Exception = None, debug: bool = False, data: dict = None, **_):
        """
        :param id: request ID (for correlation) or None if it cannot be determined
        :param data: additional metadata, which will be stored in `error.data`
        :param base_exc: base python exception object, you can pass it to trace
            the error, the data will be stored in 'error.data["traceback"]'.
        :param debug: debug mode
        """

        super().__init__(id, 'Server is closing.', base_exc=base_exc, debug=debug, data=data)


class RPCPermissionDenied(RPCError):
    """This error indicates that the RPC server doesn't allow the method execution
    for this particular user.
    """

    code = 401  #: The timeout error code is not specified in the original JSON RPC spec.
                # so I'm using a similar HTTP codes in these cases

    __slots__ = ('id', 'message', 'data', 'debug', 'base_exc')

    def __init__(self, id: Union[int, None], base_exc: Exception = None, debug: bool = False, data: dict = None, **_):
        """
        :param id: request ID (for correlation) or None if it cannot be determined
        :param data: additional metadata, which will be stored in `error.data`
        :param base_exc: base python exception object, you can pass it to trace
            the error, the data will be stored in 'error.data["traceback"]'.
        :param debug: debug mode
        """

        super().__init__(id, 'Permission denied.', base_exc=base_exc, debug=debug, data=data)
