import asyncio
from typing import *

import pytest

from ..rpc import AbstractRPCCompatibleService

__all__ = ['rpc_compatible_service']


@pytest.fixture
def rpc_compatible_service():

    class TestService(AbstractRPCCompatibleService):

        service_name = 'm'

        @property
        def routes(self) -> dict:
            return {
                'echo': self.echo,
                'aecho': self.async_echo,
                'sum': self.sum,
                'fail': self.failed,
                'long_echo': self.async_long_echo,
                'split': self.split,
                'standard_echo': self.async_standard_echo
            }

        def sum(self, x: float, y: float) -> float:
            """A sum command.

            :param x: first value
            :example x: 7
            :param y: second value
            :example y: 6
            :returns: sum of two values
            """

            return x + y

        def split(self, value: str, delimiter: str) -> List[str]:
            """Splits a string value by delimiter.

            :returns: split parts
            """

            return value.split(delimiter)

        async def failed(self):
            """A simple wrong command."""

            raise ValueError('Something bad happened.')

        def echo(self, *args, **kws):
            """A simple echo command which accepts any arguments."""

            self.logger.info('Executing echo.')
            return args, kws

        async def async_echo(self, *args, **kws):
            """A simple echo command which accepts any arguments."""

            self.logger.info('Executing async echo.')
            await asyncio.sleep(0.01)
            return args, kws

        async def async_standard_echo(self, *args, **kws):
            """A simple echo command which accepts any arguments."""

            self.logger.info('Executing long echo.')
            await asyncio.sleep(0.5)
            return args, kws

        async def async_long_echo(self, *args, **kws):
            """A simple echo command which accepts any arguments."""

            self.logger.info('Executing long echo.')
            await asyncio.sleep(3)
            return args, kws

    return TestService
