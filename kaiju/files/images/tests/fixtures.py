from pathlib import Path

import pytest


@pytest.fixture
def converter_settings():
    return {
        'ext': ['jpg', 'jpeg'],
        'filename_mask': 'image[0-9]*',
        'directory_mask': None,
        'meta': {
            'some_meta': True,
        },
        'operations': [
            {'name': 'blur'}
        ],
        'versions': [

            {
                'version': 'original',
                'format': 'png',
                'source': 'unprocessed_original'
            },

            {
                'version': 'resized',
                'format': 'jpeg',
                'source': 'unprocessed_original',
                'operations': [
                    {'name': 'resize', 'params': [200, 200]}
                ]
            },

            {
                'version': 'scaled',
                'format': 'jpeg',
                'source': 'processed_original',
                'operations': [
                    {'name': 'scale', 'params': 0.5},
                    {'name': 'flip_vertical'}
                ],
                'save_settings': {
                    'quality': 10
                }
            },

            {
                'version': 'cropped',
                'format': 'png',
                'source': 'previous_version',
                'operations': [
                    {'name': 'aspect_crop', 'params': {'ratio': [1, 1], 'rel_shift': [0.5, 0.5]}}
                ],
                'meta': {
                    'type': 'crop'
                }
            }

        ]
    }


@pytest.fixture
def test_image_file():
    return Path('./kaiju/files/images/tests/image.jpg')
