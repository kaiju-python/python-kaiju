import pytest

from ..pool import SMBConnectionPool

__all__ = [
    'smb_pool'
]


@pytest.fixture
def smb_pool(logger) -> SMBConnectionPool:

    smb_test_settings = {
        'username': 'Elemento',
        'password': '6Gd^3sE5',
        'remote_name': 'datasrv',
        'is_direct_tcp': True,
        'sign_options': 2
    }

    smb_test_connection_settings = {
        'ip': '192.168.12.60',
        'port': 445,
        'timeout': 60
    }

    return SMBConnectionPool(
        min_size=2, max_size=3,
        smb_settings=smb_test_settings,
        connection_settings=smb_test_connection_settings,
        logger=logger)
