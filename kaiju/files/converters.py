from kaiju.services import AbstractClassManager
from .abc import AbstractFileConverter
from .images.file_converter import ImageConverter

__all__ = ['Converters']


class Converters(AbstractClassManager):
    _class = AbstractFileConverter


Converters.register_class(ImageConverter)
