import asyncio
import uuid
from pathlib import Path
import os
import shutil
import tempfile
import hashlib
from concurrent.futures import ThreadPoolExecutor
from datetime import datetime, timedelta
from enum import Enum, auto
from tempfile import TemporaryDirectory
from typing import *

import aiofiles
import sqlalchemy as sa
import sqlalchemy.dialects.postgresql as sa_pg
from asyncpg.exceptions import ForeignKeyViolationError, UniqueViolationError

from kaiju.abc import Serializable
from kaiju.rpc import AbstractRPCCompatibleService
from kaiju.services import ContextableService, Service
from kaiju.db.services import SQLService
from kaiju.helpers import recursive_update, async_run_in_thread
from kaiju.rest.exceptions import NotFound, ValidationError
from .errors import ErrorCode
from .converters import Converters
from .loaders import Loaders
from .models import converters, loaders, files, photos

__all__ = [
    'FileService', 'ImageService', 'FileLoadingManagementService', 'ConverterSettings',
    'TransportSettings', 'LoaderSettings', 'FileLoadersService', 'FileConvertersService'
]


class FileService(SQLService):
    """File management service which handlers uploads and downloads."""

    service_name = 'Files'
    table = files
    URI_PREFIX = '/files/'
    TEMP_DIR = '/tmp/elemento'
    DELETE_UNLINKED_INTERVAL_DAYS = 1

    def __init__(
            self, app, dir='.', temp_dir: str = TEMP_DIR, uri_prefix: str = URI_PREFIX,
            logger=None, delete_unlinked_interval_days: int = DELETE_UNLINKED_INTERVAL_DAYS):
        super().__init__(app=app, logger=logger)
        self._dir = Path(dir).resolve()
        self._dir.mkdir(exist_ok=True, parents=True)
        self._uri_prefix = Path(uri_prefix)
        self._temp_dir = Path(temp_dir) / self.logger.name
        self._temp_dir.mkdir(exist_ok=True, parents=True)
        self.delete_unlinked_interval_days = max(1, int(delete_unlinked_interval_days))
        self.virtual_columns = {
            'uri': f"'{self._uri_prefix}' || '/' || hash || '/' || name || '.' || extension"
        }

    @property
    def temp_dir(self) -> Path:
        return self._temp_dir

    @property
    def routes(self) -> dict:
        return {
            'create': self.create_files,
            'update': self.update_files,
            'get': self.get_files,
            'list': self.list_files,
            'delete': self.delete_files,
            'delete_unlinked': self.delete_unlinked_files
            # ? how to handle upload
        }

    async def delete_unlinked_files(self, columns=None):
        """Removes all old file record which have no hash (i.e. an actual file)
         linked to them."""

        t = datetime.now() - timedelta(days=self.delete_unlinked_interval_days)
        sql = self.table.delete().where(
            sa.and_(
                self.table.c.hash == None,
                self.table.c.timestamp < t
            )
        )
        if columns:
            columns = self._sql_get_columns(columns)
            sql = sql.returning(*columns)
            return await self._engine.fetch(sql)
        else:
            await self._engine.execute(sql)

    async def get_local_file_path(self, id: uuid.UUID):
        file_info = await self.get_files(id=id, columns=['hash'])
        path = self._get_local_file_path(file_info['hash'])
        return path

    def _get_local_file_path(self, hash: Optional[uuid.UUID]) -> Optional[Path]:
        if hash:
            return self._dir / str(hash) / str(hash)

    def _get_local_file_name(
            self, name: Optional[str], hash: Optional[uuid.UUID],
            extension: Optional[str]) -> Optional[str]:
        if extension:
            return f'{name}.{extension}'
        elif name:
            return f'{name}'
        elif hash:
            return str(hash)

    def _get_file_uri(
            self, name: Optional[str], hash: Optional[uuid.UUID],
            extension: Optional[str]) -> Optional[Path]:
        name = self._get_local_file_name(name=name, hash=hash, extension=extension)
        if name:
            return self._uri_prefix / str(hash) / name

    def _get_local_symlink_path(
            self, name: Optional[str], extension: Optional[str],
            hash: Optional[uuid.UUID]) -> Optional[Path]:
        name = self._get_local_file_name(
            name=name, extension=extension, hash=hash)
        if name:
            return self._dir / str(hash) / name

    def get_temp_file_path(self) -> Path:
        temp_file_name = str(uuid.uuid4())
        return self._temp_dir / temp_file_name

    async def get_temp_dir(self, *args, **kws) -> tempfile.TemporaryDirectory:
        _dir = await async_run_in_thread(tempfile.TemporaryDirectory, *args, dir=self._temp_dir, **kws)
        return _dir

    async def get_temp_file(self, *args, **kws) -> tempfile.NamedTemporaryFile:
        temp_file = await async_run_in_thread(tempfile.NamedTemporaryFile, *args, dir=self._temp_dir, **kws)
        return temp_file

    def get_temp_file_sync(self, *args, **kws) -> tempfile.NamedTemporaryFile:
        temp_file = tempfile.NamedTemporaryFile(*args, dir=self._temp_dir, **kws)
        return temp_file

    async def delete_local_file(self, name: Union[Path, str, tempfile.TemporaryFile]):
        if isinstance(name, str):
            pass
        elif isinstance(name, Path):
            name = str(name)
        else:
            name = name.name
        path = Path(name)
        if path.exists():
            await async_run_in_thread(os.unlink, name)

    async def _delete_local_files(self, hash: Optional[uuid.UUID]):
        if hash:
            d = self._get_local_file_path(hash).parent
            if d.exists():
                await async_run_in_thread(shutil.rmtree, str(d))

    async def _upload(
            self, file_id: uuid.UUID, path: Path, hash: uuid.UUID,
            name: str, extension: str) -> (uuid.UUID, Path):
        file_path = self._get_local_file_path(hash)
        file_path.parent.mkdir(exist_ok=True, parents=True)
        uri = self._get_file_uri(name=name, hash=hash, extension=extension)
        if not file_path.exists():
            await async_run_in_thread(os.rename, str(path), str(file_path))
        else:
            await async_run_in_thread(os.unlink, str(path))
        link = self._get_local_symlink_path(name=name, hash=hash, extension=extension)

        if link.exists():
            sql = self.table.select().with_only_columns([
                self.table.c.id
            ]).where(
                sa.and_(
                    self.table.c.extension == extension,
                    self.table.c.name == name,
                    self.table.c.hash == hash
                )
            )
            data = await self._engine.fetchrow(sql)
            if data:
                await self.delete_files(id=file_id, columns=None)
                file_id = data['id']
                return file_id, uri
        else:
            await async_run_in_thread(os.symlink, str(file_path), str(link))

        data = {
            'id': file_id,
            'hash': hash,
        }
        await self.update_files(data, columns=None)
        uri = self._get_file_uri(name=name, hash=hash, extension=extension)
        return file_id, uri

    async def upload_local_file(self, id: uuid.UUID, path: Union[str, Path, tempfile.NamedTemporaryFile]):
        data = await self.get_files(id, columns=['name', 'extension'])
        hash = hashlib.md5()
        temp_file = self.get_temp_file_path()
        temp_file.parent.mkdir(exist_ok=True, parents=True)

        if isinstance(path, str):
            pass
        elif isinstance(path, Path):
            path = str(path)
        else:
            path = path.name

        async with aiofiles.open(path, "rb") as _f:
            size = 1024 ** 2
            chunk = await _f.read(size)
            while chunk:
                hash.update(chunk)
                chunk = await _f.read(size)

        hash = uuid.UUID(hash.hexdigest())
        id, uri = await self._upload(
            file_id=id, path=path, hash=hash,
            name=data['name'], extension=data['extension'])

        return {
            'id': id,
            'uri': str(uri)
        }

    async def upload_content(self, id: uuid.UUID, content):
        data = await self.get_files(id, columns=['name', 'extension'])
        hash = hashlib.md5()
        temp_file = self.get_temp_file_path()
        temp_file.parent.mkdir(exist_ok=True, parents=True)

        async with aiofiles.open(temp_file, "wb") as _f:
            async for chunk in content.iter_chunked(1024 ** 2):
                hash.update(chunk)
                await _f.write(chunk)

        hash = uuid.UUID(hash.hexdigest())
        id, uri = await self._upload(
            file_id=id, path=temp_file, hash=hash,
            name=data['name'], extension=data['extension'])

        return {
            'id': id,
            'uri': str(uri)
        }

    @staticmethod
    def _create_file(name: str, extension: str = None, meta: dict = None):
        return {
            'name': name,
            'extension': extension,
            'meta': {} if meta is None else meta
        }

    async def create_file(
            self, name: str, extension: str = None, meta: dict = None,
            columns: Optional[Union[str, List[str]]] = '*'):
        row = self._create_file(name=name, extension=extension, meta=meta)
        data = await self._create_objects([row], columns=columns)
        return next(iter(data), None)

    async def create_files(
            self, data: Union[dict, List[dict]],
            columns: Optional[Union[str, List[str]]] = '*'):

        if isinstance(data, dict):
            return await self.create_file(**data, columns=columns)

        data = [self._create_file(**row) for row in data]
        data = await self._create_objects(data, columns=columns)
        return data

    async def update_file(
            self, id, name: str = None, extension: str = None, hash: uuid.UUID = None,
            meta: dict = None, columns=None):

        data = {}
        if name:
            data['name'] = name
        if extension:
            data['extension'] = extension
        if hash:
            data['hash'] = hash
        if meta is not None:
            data['meta'] = meta

        if not data:
            return {
                'id': id
            }

        sql = self.table.update().where(self.table.c.id == id).values(data)
        if columns:
            columns = self._sql_get_columns(columns)
            sql = sql.returning(*columns)
            return await self._engine.fetch(sql)
        else:
            return await self._engine.execute(sql)

    async def update_files(
            self, data: Union[dict, List[dict]],
            columns: Optional[Union[str, List[str]]] = '*'):

        if isinstance(data, dict):
            return await self.update_file(**data, columns=columns)

        data = await asyncio.gather(*(
            self.update_file(**row, columns=columns)
            for row in data
        ))
        if columns:
            return data

    def _insert_uri(self, data):
        data['uri'] = self._get_file_uri(
            name=data['name'], hash=data['hash'], extension=data['extension'])

    async def get_files(
            self, id: Union[uuid.UUID, List[uuid.UUID]],
            columns: Optional[Union[str, List[str]]] = '*'):
        data = await super()._get_objects(id=id, columns=columns)
        return data

    async def list_files(
            self, conditions: Union[List[dict], dict] = None,
            sort: Union[Union[Dict[str, str], str], List[Union[Dict[str, str], str]]] = None,
            columns: Optional[Union[str, List[str]]] = '*',
            offset: int = 0, limit: int = 10):
        data = await super()._list_objects(
            conditions=conditions, sort=sort, columns=columns,
            offset=offset, limit=limit)
        return data

    async def delete_files(self, id: Union[uuid.UUID, List[uuid.UUID]], columns='*'):
        return await super()._delete_objects(id=id, columns=columns)


class FileConvertersService(SQLService):

    class ErrorCodes(SQLService.ErrorCodes):
        INVALID_CONVERTER_SETTINGS = 'invalid_converter_settings'

    service_name = 'FileConverters'
    table = converters
    MAX_PROCESSING_TIME = 300

    converters = Converters
    file_service = FileService

    def __init__(
            self, app, file_service: Union[FileService, str] = None,
            converters=None, max_processing_time=MAX_PROCESSING_TIME, logger=None):
        super().__init__(app=app, logger=logger)
        self.file_service = self.discover_service(self.file_service, file_service)
        if converters is not None:
            self.converters = converters
        self.max_processing_time = max(1, int(max_processing_time))

    @property
    def routes(self) -> dict:
        return {
            'spec': self.get_converter_class_specs,
            'get': self.get_converters,
            'list': self.list_converters,
            'delete': self.delete_converters,
            'create': self.create_converters,
            'update': self.update_converters,
            'call': self.convert_file
        }

    async def init_system_converters(self) -> AsyncGenerator:
        """Initializes all system file converters one by one."""

        offset, step, data = 0, 100, True
        while data:
            sql = self.table.select().with_only_columns([
                self.table.c.cls,
                self.table.c.settings
            ]).where(
                self.table.c.system == True
            ).offset(offset).limit(step)
            data = await self._engine.fetch(sql)
            for row in data:
                converter = self._init_converter(row['cls'], row['settings'])
                if isinstance(converter, ContextableService):
                    await converter.init()
                yield converter
            offset += step

    async def get_converter_class_specs(self, cls: str = None):
        if cls:
            _cls = self._get_converter_class(cls)
            return {
                'cls': cls,
                'spec': _cls.spec()
            }
        else:
            return [
                {
                    'cls': cls,
                    'spec': _cls.spec()
                }
                for cls, _cls in self.converters.items()
            ]

    async def init_converter(self, id: uuid.UUID, settings=None):
        """Returns a converter instance ready to use.

        :param settings: additional settings to update defaults
        """

        converter = await self.get_converters(id, columns='*')
        if settings:
            settings = recursive_update(converter['settings'], settings)
        else:
            settings = converter['settings']
        converter = self._init_converter(converter['cls'], settings)
        if isinstance(converter, ContextableService):
            await converter.init()
        return converter

    def _get_converter_class(self, cls: str):
        if cls not in self.converters:
            keys = self.converters.keys()
            raise NotFound(
                'Converter class doesn\'t exist.',
                key=cls, available_classes=list(keys))
        return self.converters[cls]

    def _init_converter(self, cls: str, settings: dict):
        cls = self._get_converter_class(cls)
        try:
            if issubclass(cls, Service):
                converter = cls(
                    app=self.app, logger=self.logger,
                    dir=self.file_service.temp_dir, settings=settings,
                    max_processing_time=self.max_processing_time)
            else:
                converter = cls(
                    dir=self.file_service.temp_dir, settings=settings,
                    max_processing_time=self.max_processing_time)
        except (ValueError, AttributeError, TypeError) as e:
            raise ValidationError(
                'Invalid converter settings format: "%s".' % e,
                converter_cls=cls, code=self.errors.INVALID_CONVERTER_SETTINGS)
        else:
            return converter

    async def convert_file(
            self, id: uuid.UUID, file_id: uuid.UUID, settings: dict = None,
            metadata: dict = None) -> dict:

        base_file_info = await self.file_service.get_files(
            id=file_id, columns=['name', 'extension', 'meta', 'hash'])

        base_meta = base_file_info['meta']

        if metadata is None:
            metadata = base_meta
        else:
            base_meta.update(metadata)
            metadata = base_meta

        file_path = self.file_service._get_local_file_path(base_file_info['hash'])
        if not file_path.exists():
            raise RuntimeError('Local file referenced by this id (%s) doesn\'t exist.' % file_id)
        converter = await self.init_converter(id, settings)

        result = await async_run_in_thread(
            converter.convert, file_path, return_exceptions=True,
            max_exec_time=converter.max_processing_time, **metadata)

        data = []

        if isinstance(result, Exception):
            raise result

        for f, meta in result:
            version_name = meta.get('version')
            if not version_name:
                version_name = self.converters.class_key(converter)
            output_ext = meta.get('output_extension')
            if not output_ext:
                output_ext = base_file_info['extension']
            name = meta.get('name')
            if not name:
                name = base_file_info['name']
            name = f'{name}__{version_name}'
            version = {
                'name': name,
                'extension': output_ext,
                'meta': meta
            }
            file_info = await self.file_service.create_files(version)
            try:
                file_info = await self.file_service.upload_local_file(file_info['id'], f.name)
            except Exception:
                await self.file_service.delete_local_file(f.name)
                raise
            else:
                version['file'] = file_info
                data.append(version)

        return {
            'file_id': file_id,
            'converter_id': id,
            'versions': data
        }

    async def get_converters(
            self, id: Union[uuid.UUID, List[uuid.UUID]],
            columns: Optional[Union[str, List[str]]] = '*'):
        return await super()._get_objects(id=id, columns=columns)

    async def list_converters(
            self, conditions: Union[List[dict], dict] = None,
            sort: Union[Union[Dict[str, str], str], List[Union[Dict[str, str], str]]] = None,
            columns: Optional[Union[str, List[str]]] = '*',
            offset: int = 0, limit: int = 10):
        return await super()._list_objects(
            conditions=conditions, sort=sort, columns=columns,
            offset=offset, limit=limit)

    async def delete_converters(
            self, id: Union[uuid.UUID, List[uuid.UUID]],
            columns: Optional[Union[str, List[str]]] = None):
        return await super()._delete_objects(id=id, columns=columns)

    async def _update_converter(
            self, id, row, name: str = None, system: bool = None,
            settings: dict = None, columns=None):
        cls = row['cls']
        old_settings = row['settings']
        data = {}
        if settings is not None:
            settings = recursive_update(old_settings, settings)
            self._init_converter(cls, settings)  #: testing new settings
            data['settings'] = settings
        if name:
            data['name'] = name
        if system:
            data['system'] = system
        if not data:
            return {
                'id': id
            }

        sql = self.table.update().where(self.table.c.id == id).values(data)
        try:
            if columns:
                sql = sql.returning(*columns)
                return await self._engine.fetch(sql)
            else:
                await self._engine.execute(sql)
        except ForeignKeyViolationError:
            raise ValidationError(
                'Reference object does not exists.',
                code=self.errors.REFERENCE_DOES_NOT_EXISTS)
        except UniqueViolationError:
            raise ValidationError(
                'Object already exists.',
                code=self.errors.EXISTS)

    async def update_converter(
            self, id: uuid.UUID, name: str = None, system: bool = None,
            settings: dict = None, columns: Optional[Union[str, List[str]]] = '*'):
        row = await self.get_converters(id=id, columns=['cls', 'settings'])
        columns = self._sql_get_columns(columns)
        data = await self._update_converter(
            id, row, name=name, system=system, settings=settings, columns=columns)
        return data

    async def update_converters(
            self, data: Union[dict, List[dict]],
            columns: Optional[Union[str, List[str]]] = '*'):

        if isinstance(data, dict):
            return await self.update_converter(**data, columns=columns)

        try:
            ids, data_map = [], {}
            for obj in data:
                _id = obj.pop('id')
                ids.append(_id)
                data_map[_id] = obj
        except KeyError:
            raise ValidationError(
                'Not enough values in data. ID is missing.',
                code=self.errors.NOT_ENOUGH_DATA)

        sql = self.table.select().where(
            self.table.c.id.in_(ids)
        ).with_only_columns([
            self.table.c.id,
            self.table.c.settings,
            self.table.c.cls
        ])
        rows = await self._engine.fetch(sql)
        data = (
            self._update_converter(row['id'], row, **data_map[row['id']])
            for row in rows
        )
        data = await asyncio.gather(*data)
        if columns:
            return data

    def _create_converter(self, name: str, cls: str, system: bool = False, settings: dict = None, **_):
        converter = self._init_converter(cls, settings)
        return {
            'cls': self.converters.class_key(converter),
            'name': name,
            'system': system,
            'settings': converter.settings.repr()
        }

    async def create_converter(
            self, name: str, cls: str, settings: dict, system: bool = False,
            columns: Optional[Union[str, List[str]]] = '*'):

        row = self._create_converter(name=name, cls=cls, system=system, settings=settings)
        result = await self._create_objects(row, columns)
        return next(iter(result), None)

    async def create_converters(
            self, data: Union[dict, List[dict]],
            columns: Optional[Union[str, List[str]]] = '*'):

        if type(data) is dict:
            return await self.create_converter(**data)
        else:
            data = [self._create_converter(**obj) for obj in data]
            return await self._create_objects(data, columns)


class FileLoadersService(FileConvertersService):

    # TODO: rename inherited methods

    class ErrorCodes(SQLService.ErrorCodes):
        INVALID_CONVERTER_SETTINGS = 'invalid_loader_settings'

    service_name = 'FileLoaders'
    table = loaders
    converters = Loaders

    async def convert_file(self, *args, **kws):
        # TODO: should be a specific method
        raise NotImplementedError()

    def _init_converter(self, cls: str, settings: dict):
        cls = self._get_converter_class(cls)
        try:
            if issubclass(cls, Service):
                converter = cls(app=self.app, logger=self.logger, settings=settings)
            else:
                converter = cls(settings=settings)
        except (ValueError, AttributeError, TypeError) as e:
            raise ValidationError(
                'Invalid converter settings format: "%s".' % e,
                converter_cls=cls, code=self.errors.INVALID_CONVERTER_SETTINGS)
        else:
            return converter


class ImageService(SQLService):

    class Versions(Enum):
        original = None

    service_name = 'Images'
    table = photos
    file_service = FileService
    converters_service = FileConvertersService
    loaders_service = FileLoadersService

    converter_classes = ['ImageConverter']

    def __init__(self, app, file_service=None, converters_service=None, logger=None):
        super().__init__(app=app, logger=logger)
        self._files = self.app.services.Files
        self.file_service = self.discover_service(self.file_service, file_service)
        self.converters_service = self.discover_service(
            self.converters_service, converters_service, required=False)

    @property
    def routes(self) -> dict:
        return {
            'convert': self.convert_image,
            'convert_from_file': self.convert_from_file,
            'gallery': self.get_gallery,
            'create': self.create_images,
            'delete': self.delete_images,
            'versions.delete': self.delete_versions,
            'converters.list': self.list_converters
        }

    async def list_converters(self, *args, conditions=None, **kws):
        cond = {
            'cls': self.converter_classes
        }
        if not conditions:
            conditions = cond
        else:
            conditions.update(cond)

        return await self.converters_service.list_converters(conditions=conditions, *args, **kws)

    async def _convert_file(
            self, original_id, converter_id, file_id, converter_settings,
            metadata, correlation_id, columns):

        if self.converters_service is None:
            raise RuntimeError('Converter service is not available.')

        original, converter_data = await asyncio.gather(
            self.get_images(id=original_id, columns=columns),
            self.converters_service.convert_file(
                id=converter_id, file_id=file_id, settings=converter_settings,
                metadata=metadata)
        )
        versions = converter_data['versions']
        data = [
            {
                'file_id': file_id,
                'original_id': original_id,
                'version': version['meta']['version'],
                'meta': version['meta'],
                'correlation_id': correlation_id
            }
            for version in versions
        ]
        data = await self.create_images(data, columns=columns)
        return [original, *data]

    async def convert_image(
            self, id: uuid.UUID, converter_id: uuid.UUID,
            converter_settings: dict = None, metadata: dict = None,
            columns: Optional[Union[str, List[str]]] = '*'):
        """Converts an image with a converter and saves all its versions.
        The image must be of original version.
        """

        original = await self.get_images(id=id, columns=['file_id', 'version', 'correlation_id'])
        if original['version'] != self.Versions.original.value:
            raise ValidationError('Image version must be "%s".' % self.Versions.original.value)
        file_id = original['file_id']
        correlation_id = original['correlation_id']
        return await self._convert_file(
            original_id=id, file_id=file_id, converter_id=converter_id,
            converter_settings=converter_settings, metadata=metadata,
            correlation_id=correlation_id, columns=columns)

    async def convert_from_file(
            self, file_id: uuid.UUID, converter_id: uuid.UUID,
            converter_settings: dict = None, metadata: dict = None,
            correlation_id: uuid.UUID = None,
            columns: Optional[Union[str, List[str]]] = '*'):
        """Converts file with a converter. This function will create an original
        version record as well.
        """

        original = await self.create_image(
            file_id=file_id, original_id=None, version=None, meta=metadata,
            correlation_id=correlation_id)
        original_id = original['id']
        if correlation_id is None:
            correlation_id = original_id
            sql = self.table.update().where(
                self.table.c.id == original_id
            ).values(
                correlation_id=correlation_id
            )
            await self._engine.execute(sql)
        return await self._convert_file(
                original_id=original_id, file_id=file_id, converter_id=converter_id,
                converter_settings=converter_settings, metadata=metadata,
                correlation_id=correlation_id, columns=columns)

    async def get_gallery(self, id: Union[uuid.UUID, List[uuid.UUID]]):
        """Returns a gallery with all available most recent versions of images.

        :param id: list of correlation id
        """

        if isinstance(id, uuid.UUID):
            id = [id]

        sql = self.table.select().distinct(
            self.table.c.correlation_id,
            self.table.c.version
        ).where(
            self.table.c.correlation_id.in_(id)
        ).order_by(
            self.table.c.correlation_id,
            self.table.c.version,
            self.table.c.timestamp.desc()
        )
        data = await self._engine.fetch(sql)

        files = [row['file_id'] for row in data]
        files = await self._files.get_files(id=files)
        files = {row['id']: row for row in files}

        result = {}
        order = {_id: n for n, _id in enumerate(id)}

        for row in data:
            row = dict(row)
            file_id = row['file_id']
            image_id = row['correlation_id']
            if not image_id:
                image_id = row['id']
            file_info = files[file_id]
            row['file'] = file_info
            version = row['version']
            if version is None:
                version = 'original'

            if image_id in result:
                result[image_id]['versions'][version] = row
            else:
                result[image_id] = {'id': image_id, 'versions': {version: row}}

        result = list(result.values())
        result.sort(key=lambda x: order.get(x['id'], 0))
        return result

    async def get_images(self, id: Union[uuid.UUID, List[uuid.UUID]], columns: Optional[Union[str, List[str]]] = '*'):
        """Get images by id."""

        return await super()._get_objects(id=id, columns=columns)

    async def delete_images(self, id: Union[uuid.UUID, List[uuid.UUID]], columns: Optional[Union[str, List[str]]] = '*'):
        """Removes a single image or all versions of an image (if id passed is an original id).

        :param id: list of ids, in case of original ids all image versions also will be removed
        :param columns: columns to return
        """

        return await super()._delete_objects(id=id, columns=columns)

    async def delete_versions(
            self, id: Union[uuid.UUID, List[uuid.UUID]], versions: Union[str, List[str]],
            columns: Optional[Union[str, List[str]]] = '*'):
        """Remove particular versions of images.

        :param id: list of original image ids
        :param versions: list of image versions (excl. "original")
        :param columns: columns to return
        """

        if isinstance(id, uuid.UUID):
            ids = [id]
        else:
            ids = id
        if isinstance(versions, str):
            versions = [versions]
        if self.Versions.original.value in versions:
            raise ValidationError(
                'Deleting image originals is prohibited.'
                ' Use "delete" method instead to remove the whole version tree.')
        sql = self.table.delete().where(
            sa.and_(
                self.table.c.original_id.in_(ids),
                self.table.c.version.in_(versions)
            )
        )
        if columns:
            columns = self._sql_get_columns(columns)
            sql = sql.returning(*columns)
            if isinstance(id, uuid.UUID):
                data = await self._engine.fetchrow(sql)
            else:
                data = await self._engine.fetch(sql)
            return data
        else:
            await self._engine.execute(sql)

    @staticmethod
    def _get_correlation_id():
        return uuid.uuid4()

    def _create_image(
            self, file_id: uuid.UUID, original_id: uuid.UUID = None, version: str = None,
            correlation_id: uuid.UUID = None, meta: dict = None, **_):
        if version is None:
            version = self.Versions.original.value
        if version != self.Versions.original.value:
            if not original_id:
                raise ValueError('Image of version "%s" must have "original_id".' % version)
        if correlation_id is None:
            correlation_id = self._get_correlation_id()
        data = {
            'version': version,
            'file_id': file_id,
            'original_id': original_id,
            'correlation_id': correlation_id,
            'meta': meta if meta else {}
        }
        return data

    async def create_image(
            self, file_id: uuid.UUID, original_id: uuid.UUID = None,
            correlation_id: uuid.UUID = None,
            version: str = None, meta: dict = None,
            columns: Optional[Union[str, List[str]]] = '*'):
        row = self._create_image(
            file_id=file_id, original_id=original_id, version=version,
            correlation_id=correlation_id, meta=meta)
        data = await self._create_objects([row], columns=columns)
        return next(iter(data), None)

    async def create_images(self, data: Union[dict, List[dict]], columns: Optional[Union[str, List[str]]] = '*'):
        if isinstance(data, dict):
            data = [data]
        data = [self._create_image(**row) for row in data]
        data = await self._create_objects(data, columns=columns)
        return data


class FileLoadingManagementService(AbstractRPCCompatibleService):

    service_name = 'FileLoadingManagementService'

    class _LoaderActions(Enum):
        MATCH = auto()
        UPLOAD = auto()

    class _TransportActions(Enum):
        DOWNLOAD = auto()
        MARK_FAILED = auto()
        DELETE = auto()

    PARALLEL_LOADERS = 8
    PARALLEL_TRANSPORTS = 2
    REFRESH_INTERVAL = 60

    loaders_service = FileLoadersService
    file_service = FileService

    def __init__(
            self, app, transport,
            loaders_service=None, file_service=None,
            parallel_transports: int = PARALLEL_TRANSPORTS,
            parallel_loaders: int = PARALLEL_LOADERS,
            refresh_interval: int = REFRESH_INTERVAL,
            logger=None):

        super().__init__(app=app, logger=logger)

        self.transport = self.discover_service(value=transport)
        self.loaders_service = self.discover_service(self.loaders_service, loaders_service)
        self.file_service = self.discover_service(self.file_service, file_service)

        self.parallel_transports = max(1, int(parallel_transports))
        self.parallel_loaders = max(1, int(parallel_loaders))
        self.refresh_interval = max(1, int(refresh_interval))

        self._loaders = None
        self._tasks = None
        self._closing = True
        self._running = False

    @property
    def routes(self) -> dict:
        return {
            'run': self.run
        }

    async def run(self):
        if self._running:
            return False

        self._running = True
        await self.close()
        await self.init()
        await self._run()
        await self.close()
        self._running = False

        return True

    async def init(self):
        self._tasks = []
        self._closing = False
        await self._init_loaders()

    async def close(self):
        self._closing = True
        if self._tasks:
            for task in self._tasks:
                if not task.done():
                    task.cancel()
        self._tasks = None
        self._loaders = None

    async def loop(self):
        while not self._closing:
            await self.run()
            await asyncio.sleep(self.refresh_interval)

    async def _init_loaders(self):
        self._loaders = []
        async for loader in self.loaders_service.init_system_converters():
            self._loaders.append(loader)

    async def _transport_worker(
            self, transport_queue: asyncio.Queue, loader_queue: asyncio.Queue,
            _failed_queue: asyncio.Queue, _download_queue: asyncio.Queue,
            _delete_queue: asyncio.Queue, _upload_queue: asyncio.Queue):

        # TODO: exceptions

        while not self._closing:
            action, data = await transport_queue.get()

            if action == self._TransportActions.MARK_FAILED:
                _failed_queue.get_nowait()
                try:
                    await self.transport.mark_failed(*data)
                except Exception as err:
                    self.logger.error('Error moving a file: %s', err)
                _failed_queue.task_done()

            elif action == self._TransportActions.DELETE:
                _delete_queue.get_nowait()
                try:
                    await self.transport.delete(data)
                except Exception as err:
                    self.logger.error('Error removing a file: %s', err)
                _delete_queue.task_done()

            elif action == self._TransportActions.DOWNLOAD:
                _download_queue.get_nowait()
                name, metadata, loader = data
                try:
                    f = await self.transport.download(name)
                except Exception as err:
                    self.logger.error('Error downloading "%s": %s.', name, err)
                else:
                    data = (f, name, metadata, loader)
                    await loader_queue.put((self._LoaderActions.UPLOAD, data))
                    _upload_queue.put_nowait(None)
                _download_queue.task_done()

            transport_queue.task_done()

    async def _loader_worker(
            self, loader_queue: asyncio.Queue, transport_queue: asyncio.Queue,
            _delete_queue: asyncio.Queue, _upload_queue: asyncio.Queue):

        while not self._closing:
            action, data = await loader_queue.get()

            if action == self._LoaderActions.UPLOAD:
                _upload_queue.get_nowait()
                f, name, metadata, loader = data
                try:
                    await loader.upload(f, name=name, **metadata)
                except Exception as err:
                    self.logger.error('Error processing file "%s": %s.', name, err)
                else:
                    _delete_queue.put_nowait(None)
                    await transport_queue.put((self._TransportActions.DELETE, name))
                _upload_queue.task_done()
                await self.file_service.delete_local_file(f)

            loader_queue.task_done()

    async def _run(self):

        self.logger.info('Checking if there are new files.')

        if await self.transport.has_new_files():

            self.logger.info('Initializing queues.')

            loader_queue = asyncio.Queue(maxsize=self.parallel_loaders * 2)
            transport_queue = asyncio.Queue(maxsize=self.parallel_transports * 2)

            # these ones are used only for blocking

            _delete_queue = asyncio.Queue()
            _download_queue = asyncio.Queue()
            _failed_queue = asyncio.Queue()
            _upload_queue = asyncio.Queue()

            self.logger.info('Initializing transports.')

            workers = [
                asyncio.ensure_future(
                    self._transport_worker(
                        transport_queue=transport_queue,
                        loader_queue=loader_queue,
                        _delete_queue=_delete_queue,
                        _download_queue=_download_queue,
                        _failed_queue=_failed_queue,
                        _upload_queue=_upload_queue
                    )
                )
                for _ in range(self.parallel_loaders)
            ]
            self._tasks.extend(workers)

            self.logger.info('Initializing loaders.')

            workers = [
                asyncio.ensure_future(
                    self._loader_worker(
                        loader_queue=loader_queue,
                        transport_queue=transport_queue,
                        _delete_queue=_delete_queue,
                        _upload_queue=_upload_queue
                    )
                )
                for _ in range(self.parallel_loaders)
            ]
            self._tasks.extend(workers)

            # finding appropriate converter

            self.logger.info('Performing shared directory scan.')

            async for name in self.transport.list():
                self.logger.info('Found "%s"', name)
                for loader in self._loaders:
                    metadata = await loader.match(name)
                    if metadata:
                        data = (name, metadata, loader)
                        _download_queue.put_nowait(None)
                        await transport_queue.put((self._TransportActions.DOWNLOAD, data))
                        break
                else:
                    data = (name, ErrorCode.NO_LOADER_FOUND)
                    _failed_queue.put_nowait(None)
                    await transport_queue.put((self._TransportActions.MARK_FAILED, data))

            # joining queues

            self.logger.info('Joining queues.')

            await _download_queue.join()
            await _upload_queue.join()
            await _delete_queue.join()
            await _failed_queue.join()

            # await asyncio.gather(*(
            #     transport_queue.join(),
            #     loader_queue.join()
            # ))

            self.logger.info('Finished.')


class _Settings(Serializable):
    __slots__ = ['cls', 'enabled', 'settings']

    ENABLED = True

    def __init__(self, cls: str, enabled: bool = ENABLED, settings: dict = None):
        """
        :param cls: converter class name
        :param enabled: if False then the converter will be ignored
        :param settings: custom converter settings
        """

        self.cls = cls
        self.enabled = enabled
        self.settings = settings if settings else {}

    def repr(self) -> dict:
        return {
            'cls': self.cls,
            'enabled': self.enabled,
            'settings': self.settings
        }


class TransportSettings(_Settings):
    __slots__ = ['cls', 'enabled', 'settings']


class ConverterSettings(_Settings):
    __slots__ = ['cls', 'enabled', 'settings']


class LoaderSettings(_Settings):
    __slots__ = ['cls', 'enabled', 'settings']
