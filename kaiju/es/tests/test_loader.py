from time import time

import pytest

from ..loader import ESLoaderService
from ..service import ESService
from .fixtures import *


@pytest.mark.int_benchmark
def test_es_loader_performance(
        performance_test, es_test_settings, es_test_index,
        web_application, es_document, logger):

    class Loader(ESLoaderService):

        def __init__(self, num, *args, **kws):
            self._num = num
            super().__init__(*args, **kws)

        async def load(self):
            idx = self.indices[0]
            for id in range(self._num):
                yield idx, es_document(id)

        def parse(self, idx, data):
            data['id'] = str(data['id'])
            data['percent'] = data['percent'] * 2
            return data

    async def _test(num):
        idx = es_test_index
        async with ESService(web_application, **es_test_settings, logger=logger) as es:
            web_application[es.service_name] = es
            async with Loader(
                    num, web_application, indices=[idx],
                    sender_batch_size=2000, senders=5,
                    logger=logger) as loader:
                t0 = time()
                await loader.run()
                t1 = time()
                n = await es.get_index_doc_count(idx)
                assert n > 0

            await es.delete_index(idx)

        return t1 - t0, num

    num, runs = 50000, 3
    print(f'\n\nTesting ES loader performance ({num} cycles, {runs} runs)...\n')
    dt, counter, rps = performance_test(_test, args=(num,), runs=runs)

    print(dt)
    print(f'{counter} docs')
    print(f'{round(rps, 1)} docs/sec')

    print()
