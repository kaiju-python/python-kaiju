"""Команды связанные с работой с пользователями и ролями.

create_roles - создает все доступные базовые роли объявленные в ROLES
create_users - создает пользователей, переданных из настроек приложения
ROLES - список доступных ролей
"""
from asyncpgsa.connection import SAConnection

from collections import namedtuple


__all__ = [
    'check_role_exists', 'create_user',
    'ROLES', 'Role', 'role_r', 'role_rw', 'role_rwx', 'role_rx'
]


async def _create_r_role(connection: SAConnection, name: str):
    """Создает юзера с readonly правами доступа."""

    async with connection.transaction():
        await connection.execute(f"CREATE ROLE {name} WITH NOLOGIN;")
        await connection.execute(f'GRANT USAGE ON SCHEMA public TO {name};')
        await connection.execute(f'GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO {name};')
        await connection.execute(f'GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA public TO {name};')
        await connection.execute(f'GRANT SELECT ON ALL TABLES IN SCHEMA public TO {name};')
        await connection.execute(f'ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO {name};')


async def _create_rx_role(connection, name: str):
    """Создает юзера с readonly правами доступа к текущей базе и возможностью создания новых."""

    async with connection.transaction():
        await connection.execute(f"CREATE ROLE {name} WITH CREATEDB NOLOGIN;")
        await connection.execute(f'GRANT USAGE ON SCHEMA public TO {name};')
        await connection.execute(f'GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO {name};')
        await connection.execute(f'GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA public TO {name};')
        await connection.execute(f'GRANT SELECT ON ALL TABLES IN SCHEMA public TO {name};')
        await connection.execute(f'ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO {name};')


async def _drop_role(connection, name: str):
    """Убивает роль и удаляет все ее права."""

    async with connection.transaction():
        await connection.execute(f'DROP OWNED BY {name};')
        await connection.execute(f'REVOKE ALL PRIVILEGES ON ALL TABLES IN SCHEMA public FROM {name};')
        await connection.execute(f'REVOKE ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public FROM {name};')
        await connection.execute(f'REVOKE ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public FROM {name};')
        await connection.execute(f'DROP ROLE {name};')


async def _create_rw_role(connection, name: str):
    """Создает роль с правами доступа к текущей базе и без возможности создания новых."""

    async with connection.transaction():
        await connection.execute(f"CREATE ROLE {name} WITH NOLOGIN;")
        await connection.execute(f'GRANT USAGE ON SCHEMA public TO {name};')
        await connection.execute(f'GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO {name};')
        await connection.execute(f'GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public TO {name};')
        await connection.execute(f'GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO {name};')
        await connection.execute(f'ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT, INSERT, UPDATE, DELETE ON TABLES TO {name};')


async def _create_rwx_role(connection, name: str):
    """Создает юзера с полными правами для данной базы."""

    async with connection.transaction():
        await connection.execute(f"CREATE ROLE {name} WITH CREATEDB NOLOGIN;")
        await connection.execute(f'GRANT ALL PRIVILEGES ON SCHEMA public TO {name};')
        await connection.execute(f'GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO {name};')
        await connection.execute(f'GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public TO {name};')
        await connection.execute(f'GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO {name};')
        await connection.execute(f'ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT ALL PRIVILEGES ON TABLES TO {name};')

# все группы пользователей

Role = namedtuple('db_role', 'name, init_script, comment')

role_r = Role('r', _create_r_role, 'readonly user')
role_rw = Role('rw', _create_rw_role, 'normal user')
role_rx = Role('rx', _create_rx_role, 'user for db testing')
role_rwx = Role('rwx', _create_rwx_role, 'root user')

ROLES = (role_r, role_rw, role_rx, role_rwx)  #: список доступных ролей


async def check_role_exists(connection, role: str) -> bool:
    """Проверяет, существует ли пользователь в базе."""

    result = await connection.fetchval(f"SELECT 1 FROM pg_roles WHERE rolname='{role}';")
    if result is None:
        return False
    return result == 1


async def create_user(connection, user: str, password: str, role):
    """Создает пользователя под определенную группу.
    """

    if not isinstance(role, Role):
        try:
            role = next(r for r in ROLES if r.name == role)
        except StopIteration:
            raise KeyError(
                'роли от которой наследуется новый пользователь не существует:'
                ' user=%s, role=%s' % (user, role)) from None

    return await connection.execute(
        f"""
        CREATE ROLE {user} WITH LOGIN PASSWORD '{password}'
        INHERIT IN ROLE {role.name};
        """
    )
