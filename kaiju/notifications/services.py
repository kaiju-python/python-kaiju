import uuid
from typing import *

import sqlalchemy as sa
from asyncpg import UniqueViolationError, ForeignKeyViolationError

from kaiju.rpc import AbstractRPCCompatibleWithPermissions
from kaiju.rest.exceptions import *
from kaiju.db.services import SQLService

from .models import notifications

__all__ = ['NotificationService']


class NotificationService(AbstractRPCCompatibleWithPermissions, SQLService):
    """RPC interface for notifications."""

    service_name = 'notifications'
    table = notifications

    class ErrorCodes(SQLService.ErrorCodes):
        pass

    class Permissions:
        VIEW_OTHERS_NOTIFICATIONS = 'view_others_notifications'
        MODIFY_OTHERS_NOTIFICATIONS = 'edit_others_notifications'

    @property
    def routes(self) -> dict:
        return {
            'get': self.get_notifications,
            'update': self.update_notifications,
            'create': self.create_notifications,
            'delete': self.delete_notifications,
            'list': self.list_notifications,
            'mark_read': self.mark_as_read
        }

    async def create_notifications(
            self, request, data: Union[dict, List[dict]],
            columns: Optional[Union[str, List[str]]] = '*'):

        def _create_notification(data, author):
            try:
                _data = {
                    'user_id': data['user_id'],
                    'message': data['message'],
                    'meta': data.get('meta'),
                    'task_id': data.get('task_id'),
                    'author_id': author
                }
            except KeyError:
                raise ValidationError(
                    'Not enough values in data.',
                    code=self.errors.NOT_ENOUGH_DATA)
            return _data

        author = self.get_user(request)

        if type(data) is dict:
            data = [_create_notification(data, author)]
            result = await self._create_objects(data, columns)
            return next(iter(result), None)
        else:
            data = [_create_notification(obj, author) for obj in data]
            return await self._create_objects(data, columns)

    async def update_notifications(
            self, request, id: Union[uuid.UUID, List[uuid.UUID]],
            data: dict, columns: Optional[Union[str, List[str]]] = '*'):

        def _update_notifications(data):
            _data = {}
            _update_fields = {'message', 'meta', 'active'}
            for key, value in data.items():
                if key in _update_fields:
                    _data[key] = value
            return _data

        sql = self.table.update().values(_update_notifications(data))

        author = self.get_user(request)
        if author and not self.has_permission(request, self.permissions.MODIFY_OTHERS_NOTIFICATIONS):
            sql = sql.where(self.table.c.author_id == author)

        columns = self._sql_get_columns(columns)

        try:
            if type(id) is uuid.UUID:
                sql = sql.where(self.table.c.id == id)
                if columns:
                    sql = sql.returning(*columns)
                else:
                    sql = sql.returning(self.table.c.id)
                data = await self._engine.fetchrow(sql)
                if not data:
                    raise NotFound(
                        'Notification doesn\'t exist or is not visible.',
                        key=str(id), code=self.errors.NOT_FOUND)
                if columns:
                    return data
            else:
                sql = sql.where(self.table.c.id.in_(id))
                if columns:
                    sql = sql.returning(*columns)
                    return await self._engine.fetch(sql)
                else:
                    return await self._engine.execute(sql)

        except ForeignKeyViolationError:
            raise ValidationError(
                'Reference object does not exists.',
                code=self.errors.REFERENCE_DOES_NOT_EXISTS)
        except UniqueViolationError:
            raise ValidationError(
                'Object already exists.',
                code=self.errors.EXISTS)

    async def delete_notifications(self, request, id: Union[uuid.UUID, List[uuid.UUID]]):
        sql = self.table.delete().returning(self.table.c.id)

        author = self.get_user(request)
        if author and not self.has_permission(request, self.permissions.MODIFY_OTHERS_NOTIFICATIONS):
            sql = sql.where(self.table.c.author_id == author)

        if type(id) is uuid.UUID:
            sql = sql.where(self.table.c.id == id)
            data = await self._engine.fetchrow(sql)
            if not data:
                raise NotFound(
                    'Notification doesn\'t exist or is not visible.',
                    key=str(id), code=self.errors.NOT_FOUND)
        else:
            sql = self.table.delete().where(self.table.c.id.in_(id)).returning(self.table.c.id)
            data = await self._engine.fetch(sql)
        return data

    async def get_notifications(self, request, id: Union[uuid.UUID, List[uuid.UUID]], columns='*'):
        sql = self.table.select()
        columns = self._sql_get_columns(columns)
        if columns:
            sql = sql.with_only_columns(columns)

        author = self.get_user(request)
        if author and not self.has_permission(request, self.permissions.VIEW_OTHERS_NOTIFICATIONS):
            sql = sql.where(self.table.c.author_id == author)

        if type(id) is uuid.UUID:
            sql = sql.where(self.table.c.id == id)
            data = await self._engine.fetchrow(sql)
            if not data:
                raise NotFound(
                    'Notification doesn\'t exist.',
                    key=str(id), code=self.errors.NOT_FOUND)
        else:
            sql = sql.where(self.table.c.id.in_(id))
            data = await self._engine.fetch(sql)
        return data

    async def list_notifications(
            self, request, conditions: Union[List[dict], dict] = None,
            sort: Union[Union[Dict[str, str], str], List[Union[Dict[str, str], str]]] = None,
            columns: Optional[Union[str, List[str]]] = '*',
            offset: int = 0, limit: int = 10):

        sql = self.table.select()
        sql = self._sql_paginate(sql, offset, limit)
        if sort:
            sql = self._sql_sort(sql, sort)
        else:
            sql = sql.order_by(sql, self.table.c.id)
        if conditions:
            sql = self._sql_get_conditions(sql, conditions)
        columns = self._sql_get_columns(columns)
        if columns:
            sql = sql.with_only_columns(columns)

        author = self.get_user(request)
        if author and not self.has_permission(request, self.permissions.VIEW_OTHERS_NOTIFICATIONS):
            sql = sql.where(self.table.c.author_id == author)

        return await self._engine.fetch(sql)

    async def mark_as_read(self, request, id: Union[uuid.UUID, List[uuid.UUID]]):
        user = self.get_user(request)
        sql = self.table.update().where(self.table.c.user_id == user)
        if type(id) is uuid.UUID:
            sql = sql.values(active=False).where(self.table.c.id == id).returning(self.table.c.id)
            data = await self._engine.fetchrow(sql)
            if not data:
                raise NotFound(
                    'Notification doesn\'t exist.',
                    key=str(id), code=self.errors.NOT_FOUND)
        else:
            sql = sql.values(active=False).where(
                self.table.c.id.in_(id)).returning(self.table.c.id)
            data = await self._engine.fetch(sql)
        return data
