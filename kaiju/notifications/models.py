import sqlalchemy as sa
import sqlalchemy.dialects.postgresql as sa_pg

__all__ = ['create_notifications_table', 'notifications']


def create_notifications_table(
        table_name: str, task_table_name: str, users_table_name: str,
        metadata: sa.MetaData, *columns: sa.Column):
    """
    :param table_name: custom table name
    :param task_table_name:
    :param users_table_name:
    :param metadata: custom metadata object
    :param columns: additional columns
    """

    return sa.Table(
        table_name, metadata,

        sa.Column('id', sa_pg.UUID, primary_key=True, server_default=sa.text("uuid_generate_v4()")),
        sa.Column('message', sa_pg.TEXT, nullable=False),
        sa.Column('meta', sa_pg.JSONB, nullable=True),
        sa.Column('timestamp', sa_pg.TIMESTAMP, nullable=False, server_default=sa.text("now()")),
        sa.Column('active', sa_pg.BOOLEAN, nullable=False, server_default=sa.text("TRUE")),
        sa.Column(
            'user_id',
            sa.ForeignKey(f'{users_table_name}.id', onupdate="RESTRICT", ondelete="CASCADE"),
            nullable=False),
        sa.Column(
            'author_id',
            sa.ForeignKey(f'{users_table_name}.id', onupdate="RESTRICT", ondelete="CASCADE"),
            nullable=True),
        sa.Column(
            'task_id',
            sa.ForeignKey(f'{task_table_name}.id'), nullable=True),

        *columns,

        sa.Index('idx_notification_timestamp', 'timestamp')
    )


notifications = create_notifications_table('notifications', 'tasks', 'users', sa.MetaData())
