import abc
import asyncio
import uuid
from time import time
from typing import *

import aiormq
from rapidjson import *

from kaiju.abc import Loggable, Contextable
from kaiju.rpc.jsonrpc import *
from kaiju.rpc.rpc import JSONQueueServer
from .pool import Pool


__all__ = ['AbstractRMQRPCClient']


class AbstractRMQRPCClient(Loggable, Contextable, abc.ABC):
    """The class will register itself in exchange, it will create a queue for
    requests and a callback queue for processing the responses.

    *You must define `on_message` async function with your custom processing logic.*

    Use RPCRequest, RPCResponse and RPCError objects for creating valid requests and
    responses.

    You can pass a batch of requests to `publish` method (see example). They will
    be received at once but processed separately by a consumer.
    """

    encoding = 'utf-8'                 #: content encoding, utf-8 is recommended
    content_type = 'application/json'  #: currently only plain JSON is supported

    conn_exception_classes = (
        ConnectionError, TimeoutError, RuntimeError,
        aiormq.ConnectionChannelError, asyncio.streams.IncompleteReadError)

    def __init__(
            self, pool: Pool, exchange: str, name: str = None,
            queues: Collection[str] = None, callback: Union[str, bool] = None,
            max_parallel_requests: int = 256,
            auto_ack=True, durable_queues=False,
            auto_delete_queues=True, auto_delete_exchange=True,
            task_cleaner_interval: int = 10,
            senders: int = 2, processors: int = 2, max_request_time: int = 10,
            logger=None):
        """
        :param pool: RabbitMQ connection pool
        :param exchange: exchange name
        :param name: name of the client (will be used for a personal callback if it is needed)
        :param queues: a queue list for incoming requests, if None,
            then no queues will be created
        :param callback: response callback queue (will be used in `reply_to`
            RMQ header), if None, then the name will be generated on init,
            if False, then no personal callback will be created
        :param durable_queues: queue messages will be dumped to HDD by RabbitMQ
            (slower, but persists your data)
        :param auto_delete_queues: RabbitMQ setting - auto-remove queue when
            all its users were disconnected (personal auto-generated callback
            is always has this policy and "exclusive" attr set to True)
        :param max_parallel_requests: max number of parallel requests which will
            be processed by a single client

        :param auto_ack: automatic message acknowledge on receive,
            otherwise the message will be committed only after it was processed
            by the RPC server (slower)
        :param publish_confirm: RabbitMQ setting - RabbitMQ will confirm to
            publisher what the message has been put to queue (slower)
        :param auto_delete_exchange: RabbitMQ setting - auto-remove exchange
            after all services using it exit, may cause an error if this setting
            is different from one on an already declared exchange
        :param task_cleaner_interval: interval between task cleaner scans
        :param max_request_time:

        :param args:
        :param kws:
        """

        Loggable.__init__(self, logger=logger)

        self._pool = pool
        self.exchange = exchange
        self.name = self._get_service_name(name)
        self.callback = self._get_callback_name(callback)
        self.queues = self._get_queue_names(queues)

        self.max_request_time = max(0, int(max_request_time))

        self._message_queue = None   #: a python queue for incoming messages
        self._response_queue = None  #: a queue for response objects
        self._readers = []           #: consumer tasks
        self._channels = {}          #: consumer channels
        self._tasks = []             #: all background tasks and loops
        self._task_cleaner = None    #: a separate task cleaner task
        self._ongoing_requests = {}
        self._closing = False
        self._exchange_declare = False

        self._durable_queues = bool(durable_queues)
        self._auto_delete_queues = auto_delete_queues
        self.max_parallel_requests = max(1, int(max_parallel_requests))
        self._auto_ack = bool(auto_ack)
        self._auto_delete_exchange = bool(auto_delete_exchange)
        self._task_cleaner_interval = max(0.1, float(task_cleaner_interval))
        self._senders_count = max(1, int(senders))
        self._processors_count = max(1, int(processors))

    @abc.abstractmethod
    async def on_request(
            self, headers: dict, request: RPCRequest
    ) -> Union[RPCResponse, RPCError]:
        """You need to provide a function for incoming RPC requests.

        **NOTE:** Each request object in a bulk request will be processed by a
                  separate `on_request` function in parallel, thus the *request*
                  parameter always contains a single request object

        :param headers: a dict with RabbitMQ message headers
        :param request: this function will receive a single RPC request/response object
        :returns: this function MUST return a single RPC response or error
        """

    @abc.abstractmethod
    async def on_response(
            self, headers: dict,
            responses: Union[Union[RPCResponse, RPCError], List[Union[RPCResponse, RPCError]]]
    ) -> Optional[Union[RPCMessage, List[RPCMessage]]]:
        """You need to provide a function for incoming RPC responses and errors.

        :param headers: a dict with RabbitMQ message headers
        :param responses: a single response or a bulk response object
        :returns: this function MAY return a RPC message object to send
            back to the client if reply-to was specified by it
        """

    async def init(self):
        """Inits RabbitMQ connection and starts all queue loop listener tasks."""

        self.logger.debug('Starting a new RPC client.')
        self._message_queue = asyncio.Queue(maxsize=self.max_parallel_requests)
        self._response_queue = asyncio.Queue()
        await self._init_exchange()
        self._task_cleaner = asyncio.ensure_future(self._cleaner_loop())
        for _ in range(self._processors_count):
            task = asyncio.ensure_future(self._message_processing_loop())
            self._tasks.append(task)
        for _ in range(self._senders_count):
            task = asyncio.ensure_future(self._response_loop())
            self._tasks.append(task)
        for queue in self.queues:
            task = asyncio.ensure_future(self._consumer_loop(queue))
            self._readers.append(task)
        await asyncio.sleep(0.1)
        self.logger.info(
            'Started a new RPC client. Exclusive callback=%s.', self.callback)

    async def close(self):
        """Stops all queue listeners and releases a connection back to the pool."""

        self.logger.info('Terminating RPC client.')
        self._closing = True
        for ch in self._channels.values():
            self._pool.release(ch)
        self._channels = {}
        for reader in self._readers:
            reader.cancel()
        for _ in range(self._processors_count):
            await self._message_queue.put(None)
        for _ in range(self._senders_count):
            self._response_queue.put_nowait(None)
        await asyncio.gather(
            *(task for task in self._tasks if not task.done()))
        for method, ttl, request in self._ongoing_requests.values():
            request.cancel()
        self._ongoing_requests = {}
        self._task_cleaner.cancel()
        self._task_cleaner = None
        self._tasks, self._message_queue = [], None
        self._readers = []
        self._closing = False
        self._exchange_declare = False
        self.logger.info('RPC client was terminated.')

    @property
    def closed(self):
        return bool(self._tasks)

    async def publish(
            self, queue: str, msg: Union[RPCMessage, List[RPCMessage]],
            priority: int = None, deadline: Optional[int] = None,
            headers: dict = None, reply_to: Union[str, bool] = None,
            correlation_id=None):
        """Publish a message to a queue.

        **!!!Returns an awaitable!!!**

        :param queue: queue to send a message to
        :param msg: a JSON RPC message or a batch
        :param priority: RabbitMQ priority 0-10
        :param deadline: request execution deadline (timestamp), None for no
            deadline
        :param headers: optional headers for RabbitMQ message
        :param reply_to: callback queue for request, if None - then default
            callback will be used (default behaviour), if False - then no
            reply is required
        :param correlation_id: id of a message sequence (equivalent to request_id
            in JSON RPC)
        :returns: an awaitable returning a message correlation ID or an exception object
        """

        if correlation_id:
            _correlation_id = str(correlation_id)
        else:
            _correlation_id = None
        if priority:
            priority = int(priority)
        if reply_to is None:
            reply_to = self.callback
        elif reply_to is False:
            reply_to = None

        msg = dumps(
            msg, uuid_mode=UM_CANONICAL, ensure_ascii=False,
            datetime_mode=DM_ISO8601, number_mode=NM_DECIMAL,
            allow_nan=False, default=dict).encode()

        try:
            async with self._pool.acquire() as ch:
                await self._publish(
                    ch, queue, msg, priority=priority, headers=headers,
                    reply_to=reply_to, correlation_id=_correlation_id,
                    deadline=deadline)
        except self.conn_exception_classes as err:
            raise RuntimeError(
                'A connection error during publishing. [%s]: %s.',
                err.__class__.__name__, err)
        else:
            return correlation_id

    @staticmethod
    def _encode(data) -> bytes:
        """Left for compatibility reasons."""

        return dumps(
            data, uuid_mode=UM_CANONICAL, ensure_ascii=False,
            datetime_mode=DM_ISO8601, number_mode=NM_DECIMAL,
            allow_nan=False, default=dict).encode()

    @staticmethod
    def _get_service_name(name: Union[str, None]) -> str:
        if name:
            return str(name)
        else:
            return str(uuid.uuid4())

    def _get_callback_name(self, callback: Union[str, None, bool]) -> Optional[str]:
        """Generates a personal callback queue name."""

        if callback is None:
            return self.name
        elif callback is False:
            return None
        else:
            return str(callback)

    def _get_queue_names(self, queues: Union[str, Collection[str], None]) -> List[str]:
        _queues = []
        if self.callback:
            _queues.append(self.callback)
        if queues:
            if isinstance(queues, str):
                _queues.append(str(queues))
            else:
                _queues.extend(list(queues))
        return _queues

    async def _consumer_loop(self, queue: str):
        """A consumer queue loop what listens to a RabbitMQ queue."""

        id = uuid.uuid4().int
        auto_ack = self._auto_ack
        message_queue = self._message_queue
        pool = self._pool
        channels = self._channels

        while not self._closing:
            async with pool.acquire() as ch:
                self._channels[id] = ch
                basic_consume = ch.basic_consume
                try:
                    await self._init_exchange()
                    await self._init_rmq_queue(ch, queue)
                    while 1:
                        await basic_consume(queue, message_queue.put, no_ack=auto_ack)
                except self.conn_exception_classes as err:
                    if not self._closing:
                        self.logger.error(
                            'Connection error in a consumer loop. [%s]: %s',
                            err.__class__.__name__, err)
                        pool.release(ch)
                        if id in channels:
                            del channels[id]
                        await asyncio.sleep(0.1)
                        continue
                    else:
                        return

                except asyncio.CancelledError:
                    pool.release(ch)
                    if id in channels:
                        del channels[id]
                    return

        self.logger.debug('Stopped a consumer loop %s on queue %s.', id, queue)

    async def _message_processing_loop(self):
        """Processes a message a stores a response in a response loop if needed."""

        def _process_request(msg):
            try:
                if 'error' in msg:
                    msg = RPCError(msg['id'], **msg['error'])
                elif 'result' in msg:
                    msg = RPCResponse(**msg)
                else:
                    msg = RPCRequest(**msg)
            except (TypeError, ValueError, KeyError) as err:
                msg = RPCInvalidRequest(None, base_exc=err)
            return msg

        # all in one function because it's faster

        auto_ack = self._auto_ack
        message_queue = self._message_queue
        response_queue = self._response_queue
        ongoing_requests = self._ongoing_requests
        timeout_err = TimeoutError('Request was cancelled by the RPC server.')
        max_request_time = self.max_request_time

        while 1:
            message = await message_queue.get()

            if message is None:
                message_queue.task_done()
                self.logger.debug('Stopped a message processing loop.')
                return

            decoded_headers = dict(message.header.properties)
            _headers = decoded_headers.get('headers')
            deadline = _headers.get('request-deadline') if _headers else None
            msg_id = decoded_headers['correlation_id']

            t = time()
            max_deadline = int(t + 1 + max_request_time)

            if deadline:
                deadline = min(int(deadline), max_deadline)
                if deadline < t:
                    self.logger.info(
                        'Request %s was rejected because of deadline.', msg_id)
                    message_queue.task_done()
                    continue
            else:
                deadline = max_deadline

            if msg_id:
                msg_id_str = msg_id
                msg_id = uuid.UUID(msg_id)
                msg_id_int = msg_id.int
            else:
                msg_id = uuid.uuid4()
                msg_id_str = str(msg_id)
                msg_id_int = msg_id.int

            reply_to = decoded_headers['reply_to']

            try:
                msg = loads(
                    message.body, uuid_mode=UM_CANONICAL,
                    datetime_mode=DM_ISO8601, number_mode=NM_DECIMAL,
                    allow_nan=False)
            except ValueError as err:
                if reply_to:
                    resp = RPCJSONParseError(None, base_exc=err)
                    response_queue.put_nowait((reply_to, msg_id_str, resp))
                message_queue.task_done()
                continue

            if not msg:
                if reply_to:
                    resp = RPCInvalidRequest(None)
                    response_queue.put_nowait((reply_to, msg_id_str, resp))
                message_queue.task_done()
                continue

            if isinstance(msg, list):
                msg = [_process_request(m) for m in msg]
            else:
                msg = _process_request(msg)

            if isinstance(msg, list):

                if not msg:
                    resp = RPCInvalidRequest(msg_id_int, message='Empty batch is not allowed.')

                else:

                    if isinstance(msg[0], RPCRequest):

                        result = (self.on_request(decoded_headers, req) for req in msg)
                        result = asyncio.gather(*result, return_exceptions=True)
                        result = asyncio.ensure_future(result)
                        ongoing_requests[msg_id_int] = ('batch', deadline, result)

                        try:
                            result = await result
                        except asyncio.CancelledError:
                            resp = RPCRequestTimeout(msg_id_int, base_exc=timeout_err)
                        else:
                            resp = []
                            for request, result in zip(msg, result):
                                if isinstance(result, Exception):
                                    result = RPCInternalError(request.id, base_exc=result)
                                resp.append(result)
                        finally:
                            if msg_id_int in ongoing_requests:
                                del ongoing_requests[msg_id_int]

                    else:
                        resp = await self.on_response(decoded_headers, msg)

            else:

                if isinstance(msg, RPCRequest):

                    result = self.on_request(decoded_headers, msg)
                    result = asyncio.ensure_future(result)
                    ongoing_requests[msg_id_int] = (msg.method, deadline, result)

                    try:
                        resp = await result
                    except asyncio.CancelledError:
                        resp = RPCRequestTimeout(msg.id, base_exc=timeout_err)
                    except Exception as err:
                        resp = RPCInternalError(msg.id, base_exc=err)
                    finally:
                        if msg_id_int in ongoing_requests:
                            del ongoing_requests[msg_id_int]

                else:
                    resp = await self.on_response(decoded_headers, msg)

            if resp and reply_to:
                resp = dumps(
                    resp, uuid_mode=UM_CANONICAL, ensure_ascii=False,
                    datetime_mode=DM_ISO8601, number_mode=NM_DECIMAL,
                    allow_nan=False, default=dict).encode()
                response_queue.put_nowait((reply_to, msg_id_str, resp))

            message_queue.task_done()

    async def _response_loop(self):
        """Response sender.

        Continuously sends responses from the response queue to the RabbitMQ.
        """

        response_queue = self._response_queue
        pool = self._pool
        headers = {'callback': self.callback} if self.callback else None
        Properties = aiormq.spec.Basic.Properties
        conn_exceptions = self.conn_exception_classes

        while 1:
            try:
                async with pool.acquire() as ch:

                    await self._init_exchange()
                    basic_publish = ch.basic_publish

                    while 1:
                        _response = await response_queue.get()

                        if _response is None:
                            self.logger.debug('Stopping a response loop.')
                            response_queue.task_done()
                            pool.release(ch)
                            self.logger.debug('Stopped a response loop.')
                            return

                        reply_to, msg_id_str, response = _response

                        prop = Properties(
                            app_id=self.name,
                            content_type=self.content_type,
                            content_encoding=self.encoding,
                            correlation_id=msg_id_str,
                            headers=headers)

                        await basic_publish(
                            response, exchange=self.exchange,
                            routing_key=reply_to, properties=prop)

                        response_queue.task_done()

            except conn_exceptions as err:
                self.logger.error(
                    'Connection error in a response loop. [%s]: %s',
                    err.__class__.__name__, err)
                pool.release(ch)
                response_queue.task_done()
                if self._closing:
                    break
                response_queue.put_nowait((reply_to, msg_id_str, response))
                continue

    async def _publish(
            self, channel: aiormq.Channel, queue: str, msg: bytes,
            priority: Optional[int], headers: Optional[dict],
            reply_to: Optional[str], correlation_id: Optional[str],
            deadline: Optional[int]):
        """Basic publish to queue."""

        if headers is None:
            headers = {}

        if deadline:
            headers['request-deadline'] = deadline

        if self.callback:
            headers['callback'] = self.callback

        properties = aiormq.spec.Basic.Properties(
            app_id=self.name,
            content_type=self.content_type,
            content_encoding=self.encoding,
            correlation_id=correlation_id,
            priority=priority,
            headers=headers,
            reply_to=reply_to)

        await channel.basic_publish(
            msg, exchange=self.exchange, routing_key=queue,
            properties=properties)
        return correlation_id

    async def _cleaner_loop(self):
        """Cancels requests that are running for too long."""

        dt = self._task_cleaner_interval
        ongoing_requests = self._ongoing_requests

        while 1:
            if ongoing_requests:
                t = time()
                for task_id, (m, deadline, task) in ongoing_requests.items():
                    if deadline and deadline < t:
                        task.cancel()
            await asyncio.sleep(dt)
