import asyncio
import inspect
import uuid
from collections import ChainMap
from datetime import datetime
from typing import *

from kaiju.rpc.jsonrpc import *
from .pool import RabbitMQPoolService
from .client import AbstractRMQRPCClient
from kaiju.services import ContextableService

__all__ = ['RPCException', 'RPCService']


class RPCException(RuntimeError):
    """A python exception class from an RPC error."""

    def __init__(self, err: RPCError, response: Union[List[RPCMessage], RPCMessage]):
        self.error = err
        self.response = response

    def __repr__(self):
        return f'{self.error.code}({self.error.message})'

    def __str__(self):
        return self.error.message


class RPCService(AbstractRMQRPCClient, ContextableService):
    """An RPC service.

    It can both send and receive and process messages.
    """

    HIST_SIZE_VARIATION = 0.25
    service_name = 'rpc'

    def __init__(
            self, *args, methods: Dict[str, Any] = None,
            unidentified_response_hist_size=0,
            max_request_timeout=60, app=None, pool=None, **kws):
        """
        :param methods: dict with { method_name: method } mapping, both awaitable
            and normal functions are allowed
        :param unidentified_response_hist_size: queue size for unidentified
            responses
            0 for no history - all unidentified data will be skipped
            -1 for infinite history (in this case you need a manager to process
            `unidentified_responses` queue by yourself to prevent memory leak)
        :param max_request_timeout: max awaiting request timeout in seconds,
            0 for no timeout (requests will wait forever)
        :param app: you may pass an application instance that will be passed in each of
            RPC methods on call
        :param args:
        :param kws:
        """

        if pool is None:
            pool = app[RabbitMQPoolService.service_name]

        AbstractRMQRPCClient.__init__(
            self, pool=pool, *args, **kws)
        ContextableService.__init__(self, app)

        self._coros, self._functions = {}, {}
        self._methods = ChainMap(self._coros, self._functions)
        self._register_method('', 'list', self._list_methods)
        self._register_method('', 'help', self._method_help)
        self._register_method('', 'jobs', self._list_jobs)
        self._register_method('', 'abort', self._abort_task)

        if methods:
            for namespace, _methods in methods.items():
                for method_name, func in _methods.items():
                    self.register_method(namespace, method_name, func)

        self._awaiting, self._ttl, self._unidentified = {}, {}, None
        self._unidentified_size = int(unidentified_response_hist_size)
        _delta = max(1, int(unidentified_response_hist_size * RPCService.HIST_SIZE_VARIATION))
        self._unidentified_max_size = int(unidentified_response_hist_size) + _delta
        self._max_request_timeout = max_request_timeout
        self._ttl_loop = None

    def register_method(self, namespace: str, name: str, func: Callable):
        if namespace.startswith('rpc'):
            raise ValueError('Namespace "rpc" is reserved by an RPC .')
        elif namespace == '':
            raise ValueError('Namespace "." is reserved for rpc service internal methods.')
        self._register_method(namespace, name, func)

    def register_namespace(self, namespace: str, routes: dict):
        for method_name, func in routes.items():
            self.register_method(namespace, method_name, func)

    def _register_method(self, namespace: str, name: str, func: Callable):
        full_name = f'{namespace}.{name}'
        if full_name in self._methods:
            raise KeyError('RPC method %s is already registered.' % full_name)
        if inspect.iscoroutinefunction(func):
            self._coros[full_name] = func
        else:
            self._functions[full_name] = func
        self.logger.info('Registered new RPC method - "%s".', full_name)

    def _abort_task(self, id: uuid.UUID):
        """Instantly aborts the task execution if possible."""

        if id.int in self._ongoing_requests:
            method_name, deadline, task = self._ongoing_requests.pop(id.int)
            task.cancel()
            return {'id': id, 'method': method_name, 'state': 'aborted'}

    def _list_jobs(self):
        """Lists all ongoing requests."""

        jobs = []

        for r, (m, deadline, task) in self._ongoing_requests.items():
            if deadline:
                deadline = datetime.fromtimestamp(deadline)
            jobs.append({
                'id': r,
                'method': m,
                'deadline': deadline
            })

        return {'jobs': jobs}

    def _list_methods(self):
        """Lists all available RPC method names."""

        return {'methods': list(self.methods)}

    def _method_help(self, method: str):
        """Returns a method docstring and its argument specification repr."""

        m = self._methods.get(method)
        if m:
            return {
                'method': method,
                'doc': inspect.getdoc(m),
                'spec': inspect.getfullargspec(m).__repr__()
            }

    @property
    def unidentified_responses(self) -> Optional[asyncio.Queue]:
        """Returns an unidentified responses queue. The queue may contain some
        responses with `null` ID or responses with ID which isn't stored in
        the queues.

        If `unidentified_response_hist_size` was set to zero upon the RPC service
        creation, the property will return None.
        """

        return self._unidentified

    @property
    def methods(self) -> dict:
        """Returns a tuple of registered RPC method names."""

        return dict(self._methods)

    def __contains__(self, request_id: uuid.UUID):
        return request_id.int in self._awaiting

    def __len__(self):
        return len(self._awaiting)

    def __getitem__(self, request_id: uuid.UUID):
        return self._awaiting[request_id.int]

    def __setitem__(self, request_id: uuid.UUID, deadline: int):
        self._awaiting[request_id.int] = asyncio.LifoQueue()
        self._ttl[request_id.int] = deadline

    def __delitem__(self, request_id: uuid.UUID):
        if request_id in self:
            del self._awaiting[request_id.int]
            del self._ttl[request_id.int]

    async def call(self, *args, **kws):
        """Alias for call_noblock plus waiting for a response."""

        result = await self.call_noblock(*args, **kws)
        return await result

    async def call_noblock(
            self, service: str, method: str, params: Union[dict, list] = None,
            priority=None, request_id: uuid.UUID = None,
            request_timeout: int = None, raw_response=False
    ) -> Awaitable:
        """Call a method with arguments on a remote server without blocking
        the client. The function will return an awaitable object containing a
        response (when it's done), but you can just wait for response somewhere
        else in your code.

        **ATTENTION**: if you don't need a response at all, use `call_nowait`
        method which doesn't create a callback queue.

        :param service: remote service (i.e. queue) name
        :param method: remote service method name
        :param params: remote service method args (keyword or positional)
        :param priority: message processing priority on a server, the default is 0
        :param request_timeout: maximum time the client will wait for request
            if None, then the default value will be taken
        :param request_id: optional correlation id, if None, then the ID will be
            randomly generated
        :return: an awaitable callback object
        """

        msg = RPCRequest(None, method, params)
        if request_id is None:
            request_id = msg.uuid
        deadline = self._get_request_deadline(request_timeout)
        self[request_id] = request_timeout
        result = await self.publish(
            service, msg, priority=priority, deadline=deadline,
            correlation_id=request_id)
        if isinstance(result, Exception):
            del self[request_id]
            raise result
        if raw_response:
            callback = self._return_callback_raw(request_id)
        else:
            callback = self._return_callback(request_id)
        return callback

    def _get_request_deadline(self, request_timeout: int) -> int:
        if request_timeout is None:
            request_timeout = self._max_request_timeout
        deadline = int(datetime.now().timestamp() + request_timeout)
        return deadline

    async def call_nowait(
            self, service: str, method: str, params: Union[dict, list] = None,
            priority=None, request_id: uuid.UUID = None, do_not_reply=True,
            request_timeout: int = None
    ) -> int:
        """Call a method with arguments on a remote server with no wait. The
        request will be sent as soon as possible. The result won't be returned.
        This method doesn't block the client's code execution.

        **NOTE**: You still can get the result if you set `unidentified_response_hist_size`
        to more than zero and fetch data from `unidentified_responses` queue
        comparing response IDs to the one you've received from `call_nowait`
        function. However if `do_not_reply` was set to True, then the server will
        never reply to a call anyway.

        **ATTENTION**: if you need a response object, use `call_noblock`
        method.

        :param service: remote service (i.e. queue) name
        :param method: remote service method name
        :param params: remote service method args (keyword or positional)
        :param priority: message processing priority on a server, the default is 0
        :param request_timeout: maximum time the client will wait for request
            if None, then the default value will be taken
        :param request_id: optional request id, if None, then the ID will be
            randomly generated
        :param do_not_reply: server will not reply to a client
        :return: correlation id
        """

        msg = RPCRequest(None, method, params)
        if request_id is None:
            request_id = msg.uuid
        reply_to = None if do_not_reply is False else False
        deadline = self._get_request_deadline(request_timeout)
        result = await self.publish(
            service, msg, priority=priority, deadline=deadline,
            correlation_id=request_id, reply_to=reply_to)
        if isinstance(result, Exception):
            raise result
        return request_id

    async def call_multiple(self, *args, **kws):
        """Alias for call_multiple_noblock plus waiting for a response."""

        result = await self.call_multiple_noblock(*args, **kws)
        return await result

    async def call_multiple_noblock(
            self, service: str, calls: List[Tuple[str, Union[dict, list]]],
            priority=None, request_id: uuid.UUID = None, request_timeout: int = None,
            raw_response=False
    ) -> Awaitable:
        """Call multiple methods on a service in one batch non-blocking. This
        method is similar to `call_noblock`. The only difference is that you
        will receive a list of responses in the future when it's finished.

        **ATTENTION**: although service guarantees that the ordering of results
        in the response batch will match the ordering of requests in a request
        batch (and in the ID list), the method calls may be performed
        in different order on a server itself.

        :param service: remote service (i.e. queue) name
        :param calls: list of (method name, method arguments) tuples
        :param priority: message processing priority on a server, the default is 0
        :param request_timeout: maximum time the client will wait for request
            if None, then the default value will be taken
        :param request_id: optional correlation id (if None - then it will
            be autogenerated
        :return: an awaitable callback object
        """

        msg = [RPCRequest(None, method, params) for method, params in calls]
        if request_id is None:
            request_id = msg[0].uuid
        deadline = self._get_request_deadline(request_timeout)
        self[request_id] = deadline
        result = await self.publish(
            service, msg, priority=priority, deadline=deadline,
            correlation_id=request_id)
        if isinstance(result, Exception):
            del self[request_id]
            raise result
        if raw_response:
            callback = self._return_callback_raw(request_id)
        else:
            callback = self._return_callback(request_id)
        return callback

    async def call_multiple_nowait(
            self, service: str, calls: List[Tuple[str, Union[dict, list]]],
            priority=None, request_id: uuid.UUID = None, request_timeout: int = None,
            do_not_reply=True
    ) -> int:
        """Call multiple methods on a service in one batch without waiting. This
        method is similar to `call_nowait`. The only difference is that you're
        sending multiple requests in one batch.

        **NOTE**: You still can get the result if you set `unidentified_response_hist_size`
        to more than zero and fetch data from `unidentified_responses` queue
        comparing response IDs to the one you've received from `call_multiple_nowait`
        function.

        **ATTENTION**: although service guarantees that the ordering of results
        in the response batch will match the ordering of requests in a request
        batch (and in the ID list), the method calls may be performed
        in different order on a server itself.

        :param service: remote service (i.e. queue) name
        :param calls: list of (method name, method arguments) tuples
        :param priority: message processing priority on a server, the default is 0
        :param request_timeout: maximum time the client will wait for request
            if None, then the default value will be taken
        :param request_id: optional correlation id (if None - then it will
            be autogenerated
        :param do_not_reply: server will not reply to a client
        :return: correlation_id
        """

        msg = [RPCRequest(None, method, params) for method, params in calls]
        if request_id is None:
            request_id = msg[0].uuid
        reply_to = None if do_not_reply is False else False
        deadline = self._get_request_deadline(request_timeout)
        result = await self.publish(
            service, msg, priority=priority, deadline=deadline,
            correlation_id=request_id, reply_to=reply_to)
        if isinstance(result, Exception):
            raise result
        return request_id

    async def _check_queue_ttl(self):
        """Periodically checks awaiting requests for timeouts."""

        while not self._closing:
            if self._awaiting:
                t = datetime.now().timestamp()
                ttl_id = (id for id, ttl in self._ttl.items() if t > ttl)
                for id in ttl_id:
                    timeout = TimeoutError('Request timeout.')
                    self._awaiting[id].put_nowait((None, timeout))
                    del self[id]
            await asyncio.sleep(0.5)

    async def init(self):
        self._ttl_loop = asyncio.ensure_future(self._check_queue_ttl())
        self._awaiting, self._ttl = {}, {}
        if self._unidentified_size:
            self._unidentified = asyncio.Queue()  # no max size because it is checked by a function
        else:
            self._unidentified = None
        await super().init()
        self.logger.info(
            'Initialized an RPC service with available methods: %s.',
            list(self.methods.keys()))
        self.logger.info('Listening queues: %s.', self.queues)
        self.logger.info('Exclusive callback queue name: %s.', self.callback)

    async def close(self):
        self._ttl_loop.cancel()
        self._ttl_loop = None
        await super().close()
        self._awaiting, self._ttl, self._unidentified = {}, {}, None

    @property
    def closed(self):
        return super().closed

    @staticmethod
    def _eval_exception(error: RPCError, response):
        _data = error.data
        if _data and 'traceback' in _data:
            exc = eval(_data['traceback']['repr'])
            return exc
        return RPCException(error, response)

    async def _return_callback_raw(self, request_id: uuid.UUID):
        queue = self._awaiting[request_id.int]
        headers, responses = await queue.get()
        queue.task_done()
        del self[request_id]
        return headers, responses

    async def _return_callback(self, request_id: uuid.UUID):
        """This function is returned to a client when he uses `call_noblock`
        or similar method. Afterwards the client need to await this function.

        :raises: if the remote RPC service returns an exception object, then
            this callback will raise a python Exception from the RPC error
        :returns: normally it returns a "result" value from an object
        """

        queue = self._awaiting[request_id.int]
        headers, response = await queue.get()
        queue.task_done()
        del self[request_id]

        err, _results = None, []

        if isinstance(response, list):
            for r in response:
                if isinstance(r, RPCError):
                    err = self._eval_exception(r, response)
                    break
                else:
                    _results.append(r.result)
        elif isinstance(response, RPCError):
            err = self._eval_exception(response, response)
        elif isinstance(response, Exception):
            err = response
        else:
            err, _results = None, response.result

        if err:
            raise err

        return _results

    async def on_request(self, headers: dict, request: RPCRequest) -> Any:
        """On request object this func will call a related method and return its
        result.

        On response it will do nothing and just passes it back to the parent
        processing function.
        """

        try:
            result = await self._call_method(request.method, request.params)
        except asyncio.CancelledError:
            raise
        except TypeError as exc:
            return RPCInvalidParams(request.id, base_exc=exc)
        except Exception as exc:
            return RPCInternalError(request.id, base_exc=exc)
        else:
            return RPCResponse(request.id, result=result)

    async def on_response(
            self, headers: dict,
            responses: Union[Union[RPCResponse, RPCError], List[Union[RPCResponse, RPCError]]]
    ):
        """This function doesn't return response back, but stores it in an
        awaiting queue if queue with such message id exists. Otherwise the message
        will go into `unidentified_responses` queue, where it still can be
        reached until queue is full.

        When `unidentified_responses` is full, `_free_unidentified_queue` script
        is executed, voluntarily freeing some space of unidentified responses.
        """

        request_id = headers['correlation_id']

        if request_id:
            request_id = uuid.UUID(request_id)
            if request_id in self:
                self._awaiting[request_id.int].put_nowait((headers, responses))
                return

        if self._unidentified:
            if self._unidentified_size != -1:
                self._free_unidentified_queue()
            self._unidentified.put_nowait((headers, responses))

    def _free_unidentified_queue(self):
        qsize = self._unidentified.qsize()
        if qsize > self._unidentified_max_size:
            for _ in range(qsize - self._unidentified_size):
                self._unidentified.get_nowait()

    async def _call_method(self, name, request_params):

        if name not in self._methods:
            raise NameError(name)

        method = self._methods[name]

        if request_params is None:
            result = method()
        elif isinstance(request_params, dict):
            result = method(**request_params)
        elif isinstance(request_params, list):
            result = method(*request_params)
        else:
            result = method(request_params)
        if name in self._coros:
            result = await result
        return result
