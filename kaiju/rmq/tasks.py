from datetime import datetime

import aredis

from .service import RPCService
from kaiju.rpc.jsonrpc import RPCResponse

__all__ = ['TaskExecutor', 'lock_task', 'unlock_task']


async def lock_task(redis_client, id, deadline=-1):
    task_id_int = str(id.int)
    _deadline = await redis_client.get(task_id_int)
    t = datetime.now().timestamp()
    if _deadline:
        _deadline = int(_deadline)
        if _deadline == -1 or _deadline >= t:
            raise RuntimeError('Task[%s] is already locked.' % id)
    dt = max(-1, round(deadline - t + 1))
    await redis_client.set(task_id_int, deadline)
    await redis_client.expire(task_id_int, dt)


async def unlock_task(redis_client, id):
    try:
        await redis_client.delete(id.int)
    finally:
        pass


class TaskExecutor(RPCService):
    """A basic RPC task executor."""

    service_name = 'tasks'

    def __init__(self, *args, redis: aredis.StrictRedis = None, **kws):
        self._redis = redis
        super().__init__(*args, **kws)
        self.register_method('task', 'exec', self._execute_task)
        self.register_method('task', 'abort', self._abort_executor_task)

    async def _abort_executor_task(self, id):
        self.logger.debug('Aborting execution of Task[%s].', id)
        super()._abort_task(id)
        self.logger.debug('Unlocking Task[%s].', id)
        await unlock_task(self._redis, id)
        self.logger.debug('Aborted execution of Task[%s].', id)
        return {
            'id': id,
            'state': 'finished',
            'exit_code': 1,
            'log': {'error': TimeoutError('Aborted by a task manager call.').__repr__()}
        }

    async def _execute_task(self, id, method, params):
        self.logger.debug('Starting Task[%s] execution of method %s.', id, method)
        result = await self._call_method(method, params)
        self.logger.info('Finished Task[%s] execution of method %s.', id, method)
        return {
            'id': id,
            'state': 'finished',
            'result': result
        }

    async def on_request(self, headers: dict, request):
        task_id = request.params['id']
        deadline = headers['headers'].get('request-deadline')

        if deadline:
            deadline = int(deadline)

        self.logger.debug(
            'Ongoing request for %s and Task[%s].',
            request.method, task_id)

        if self._redis and request.method == 'task.exec':
            self.logger.debug('Locking Task[%s].', task_id)
            await lock_task(self._redis, task_id, deadline)
            self.logger.debug('Locked Task[%s].', task_id)

        reply_to = headers['reply_to']
        msg_id = headers['correlation_id']

        async with self._pool.acquire() as ch:

            if reply_to:
                data = {
                    'id': task_id,
                    'method': request.method,
                    'state': 'executed'
                }
                data = RPCResponse(request.id, data)
                data = self._encode(data)
                await self._publish(
                    ch, reply_to, data, priority=1, reply_to=None, correlation_id=msg_id,
                    headers=None, deadline=None)

            data = await super().on_request(headers, request)

            if reply_to:
                data.id = None
                data = self._encode(data)
                await self._publish(
                    ch, reply_to, data, priority=None, reply_to=None, correlation_id=None,
                    headers=None, deadline=None)

        self.logger.debug('Unlocking Task[%s].', task_id)
        await unlock_task(self._redis, task_id)
        self.logger.debug('Unlocked Task[%s].', task_id)
