"""Intended for user testing."""

import asyncio

import aiormq
import pytest

from kaiju.json import encoder, decoder

from .. import *
from .fixtures import *


@pytest.mark.usertest
async def test_rmq_client_manually(rabbit_test_dsn, random_string, logger):

    class Client(AbstractRMQRPCClient):

        async def on_request(self, headers, request):
            self.logger.info('<- MESSAGE')

        async def on_response(self, headers, responses):
            pass

    async with Pool(rabbit_test_dsn, conn_retries=-1, logger=logger) as pool:
        async with Client(pool=pool, exchange=random_string(8), logger=logger) as client:
            while 1:
                msg = RPCRequest(None, 'shit', {'test': True})
                logger.info('MESSAGE ->')
                await client.publish(client.callback, msg)
                await asyncio.sleep(3)


@pytest.mark.usertest
async def test_rmq_pool_manually(rabbit_test_dsn, random_string, logger):

    async def on_message(msg):
        return

    async with Pool(rabbit_test_dsn, conn_retries=-1, logger=logger) as pool:
        async with pool.acquire() as channel:
            qname = random_string(8)
            await channel.exchange_declare(
                durable=False, auto_delete=True,
                exchange='pytest', exchange_type='direct')
            await channel.queue_declare(
                qname, durable=False, auto_delete=True, exclusive=True)
            while 1:
                msg = {'test': True}
                msg = encoder(msg).encode()
                try:
                    await channel.basic_publish(
                        msg, exchange='pytest', routing_key=qname,
                        properties=aiormq.spec.Basic.Properties(
                            content_type='application/json',
                            content_encoding='utf-8'))
                except asyncio.streams.IncompleteReadError:
                    logger.info('[ READ ERROR ]')
                except RuntimeError:
                    pool.release(channel)
                    channel = await pool.get()
                    await channel.exchange_declare(
                        durable=False, auto_delete=True,
                        exchange='pytest', exchange_type='direct')
                    await channel.queue_declare(
                        qname, durable=False, auto_delete=True, exclusive=True)
                else:
                    logger.info('MESSAGE ->')
                try:
                    await channel.basic_consume(qname, on_message)
                except asyncio.streams.IncompleteReadError:
                    logger.info('[ READ ERROR ]')
                except RuntimeError:
                    pool.release(channel)
                    channel = await pool.get()
                    await channel.exchange_declare(
                        durable=False, auto_delete=True,
                        exchange='pytest', exchange_type='direct')
                    await channel.queue_declare(
                        qname, durable=False, auto_delete=True, exclusive=True)
                else:
                    logger.info('<- MESSAGE')
                await asyncio.sleep(3)
