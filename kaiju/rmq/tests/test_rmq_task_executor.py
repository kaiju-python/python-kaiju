"""Tests for RabbitMQ simple task executor with lock mechanics.
"""

import asyncio
import uuid

import pytest

from ..pool import Pool
from ..service import RPCService
from ..tasks import TaskExecutor
from .fixtures import *


@pytest.mark.integration
async def test_rmq_tasks_executor(rabbit_test_dsn, redis_client, random_string, logger):

    async def do_good(*args, **kws):
        await asyncio.sleep(1)
        return True

    async def do_long(*args, **kws):
        await asyncio.sleep(5)
        return True

    async def do_bad(*args, **kws):
        await asyncio.sleep(1)
        raise KeyError('Something shitty happened.')

    methods = {
        'do': {
            'good': do_good,
            'bad': do_bad,
            'long': do_long
        }
    }

    async with Pool(rabbit_test_dsn, logger=logger) as pool:
        exchange = random_string(8)
        async with RPCService(
            pool=pool, exchange=exchange, unidentified_response_hist_size=10,
            auto_delete_exchange=True, max_request_timeout=5,
            logger=logger
        ) as client:
            async with TaskExecutor(
                pool=pool, exchange=exchange, redis=redis_client, queues=['tasks'],
                unidentified_response_hist_size=10,
                auto_delete_exchange=True, methods=methods,
                task_cleaner_interval=0.1, logger=logger
            ) as executor:

                logger.info('Executing basic task.')

                task_id = uuid.uuid4()
                notification = await client.call(
                    'tasks', 'task.exec',
                    {
                        'id': task_id,
                        'method': 'do.good',
                        'params': [1, 2, 3]
                    }, request_id=task_id)
                assert task_id == notification['id']
                assert notification['state'] == 'executed'

                logger.info('Executing another task.')

                with pytest.raises(RuntimeError):
                    await client.call(
                        'tasks', 'task.exec',
                        {
                            'id': task_id,
                            'method': 'do.good',
                            'params': [1, 2, 3]
                        }, request_id=task_id)
                headers, response = await client.unidentified_responses.get()
                result = response.result
                assert result['id'] == task_id
                assert result['result'] is True
                assert result['state'] == 'finished'

                logger.info('Testing task cancellation.')

                await client.call(
                    'tasks', 'task.exec',
                    {
                        'id': task_id,
                        'method': 'do.long',
                        'params': [1, 2, 3]
                    }, request_id=task_id,
                    request_timeout=100)
                await asyncio.sleep(0.01)
                result = await client.call(
                    'tasks', 'task.abort',
                    {
                        'id': task_id
                    }, request_timeout=1)
                assert result['id'] == task_id
                assert result['state'] == 'executed'
                assert result['method'] == 'task.abort'

                headers, response = await client.unidentified_responses.get()
                result = response.result
                assert result['id'] == task_id
                assert result['state'] == 'finished'

                logger.info('Finished tests.')
