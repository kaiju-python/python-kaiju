"""Fixtures for rabbitmq functions testing."""

import aredis
import pytest

__all__ = [
    'rabbit_test_dsn', 'rabbit_test_ports', 'rabbit_test_settings', 'rabbit',
    'redis_client', 'redis_test_settings'
]


@pytest.fixture(scope='session')
def redis_test_settings():
    return {
        'host': '127.0.0.1',
        'port': 6379,
        'db': 0
    }


@pytest.fixture(scope='session')
def redis_client(redis_test_settings):
    return aredis.StrictRedis(**redis_test_settings)


@pytest.fixture(scope='session')
def rabbit_test_ports():
    """Test port mapping for rabbit container."""

    return {
        '5672': '5672',
        '5671': '5671'
    }


@pytest.fixture(scope='session')
def rabbit_test_settings():
    """Test environment settings for rabbit container."""

    return {
        'RABBITMQ_DEFAULT_USER': 'assistant',
        'RABBITMQ_DEFAULT_PASS': 'assistant',
        'RABBITMQ_DEFAULT_VHOST': '/',
        'RABBITMQ_NODENAME': 'rabbit@localhost',
        'RABBITMQ_ERLANG_COOKIE': 'test',
        'RABBITMQ_HIPE_COMPILE': 0
    }


@pytest.fixture(scope='session')
def rabbit_test_dsn(rabbit_test_settings, rabbit_test_ports):
    """RabbitMQ DSN string."""

    usr = rabbit_test_settings['RABBITMQ_DEFAULT_USER']
    psw = rabbit_test_settings['RABBITMQ_DEFAULT_PASS']
    host = 'localhost'
    port = rabbit_test_ports['5672']
    return f'amqp://{usr}:{psw}@{host}:{port}/'


@pytest.fixture
def rabbit(container, rabbit_test_settings, rabbit_test_ports):
    """Test rabbit container."""

    return container(
        'rabbitmq', '3.8-rc-alpine',
        ports=rabbit_test_ports,
        env=rabbit_test_settings,
        healthcheck={
            'test': "nc -z 0.0.0.0 5672",
            'interval': 10000000,
            'timeout': 1000000000,
            'start_period': 100000000,
            'retries': 1
        })
