import pytest

from kaiju.services import ServiceContextManager
from ..abc import AbstractRPCCompatibleService
from ..service import RPCService
from ..pool import RabbitMQPoolService
from .fixtures import *


@pytest.mark.integration
async def test_rpc_compatible_service(
        aiohttp_server, web_application,
        rabbit_test_dsn, random_string, logger):

    class SomeRPCService(AbstractRPCCompatibleService):

        service_name = 'some'

        def __init__(self, *args, value=None, **kws):
            self.value = value
            super().__init__(*args, **kws)

        async def some_method(self, *_, **__):
            return self.value

        @property
        def routes(self) -> dict:
            return {
                'method': self.some_method
            }

    app = web_application
    exchange = random_string(8)
    value = 42

    app.cleanup_ctx.extend([
        RabbitMQPoolService.cleanup_ctx({
            'dsn': rabbit_test_dsn
        }),
        RPCService.cleanup_ctx({
            'exchange': exchange,
            'queues': ['server'],
            'unidentified_response_hist_size': 10,
            'auto_delete_exchange': True,
            'task_cleaner_interval': 0.1
        }),
        SomeRPCService.cleanup_ctx({
            'value': value
        }),
        RPCService.cleanup_ctx({
            'exchange': exchange,
            'unidentified_response_hist_size': 10,
            'auto_delete_exchange': True,
            'task_cleaner_interval': 1
        }, service_name='rpc2'),
    ])

    await aiohttp_server(app)

    result = await app['rpc2'].call('server', 'some.method', [1, 2, 3])
    logger.debug(result)
    assert result == value


@pytest.mark.integration
async def test_rpc_compatible_service_context_manager(
        aiohttp_server, web_application,
        rabbit_test_dsn, random_string, logger):

    class SomeRPCService(AbstractRPCCompatibleService):

        service_name = 'some'

        def __init__(self, *args, value=None, **kws):
            self.value = value
            super().__init__(*args, **kws)

        async def some_method(self, *_, **__):
            return self.value

        @property
        def routes(self) -> dict:
            return {
                'method': self.some_method
            }

    app = web_application
    exchange = random_string(8)
    value = 42

    ctx = [
        {
            'class': 'RabbitMQPoolService',
            'enabled': True,
            'settings': {
                'dsn': rabbit_test_dsn
            }
        },
        {
            'class': 'RPCService',
            'enabled': True,
            'settings': {
                'exchange': exchange,
                'queues': ['server'],
                'unidentified_response_hist_size': 10,
                'auto_delete_exchange': True,
                'task_cleaner_interval': 0.1
            }
        },
        {
            'class': 'SomeRPCService',
            'enabled': True,
            'settings': {
                'value': value
            }
        },
        {
            'class': 'RPCService',
            'name': 'rpc2',
            'enabled': True,
            'settings': {
                'exchange': exchange,
                'unidentified_response_hist_size': 10,
                'auto_delete_exchange': True,
                'task_cleaner_interval': 1
            }
        }
    ]

    app.cleanup_ctx.extend(ServiceContextManager(ctx))
    await aiohttp_server(app)

    result = await app['rpc2'].call('server', 'some.method', [1, 2, 3])
    result2 = await app['some'].some_method()
    logger.debug(result)
    assert result == value
    assert result == result2

    logger.info('finished tests')
