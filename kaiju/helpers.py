import abc
import asyncio
import ctypes
import random
import string
from concurrent.futures import ThreadPoolExecutor
from concurrent.futures import TimeoutError as ConcurrentTimeoutError
from collections import Counter, defaultdict
from functools import partial
from itertools import chain, zip_longest
from hashlib import md5
from types import FunctionType
from typing import *

import multidict
from datemath import dm
from rapidjson import *

__all__ = [
    'repeat', 'random_string', 'register',

    # dictionary/mapping operations
    'wrap_secrets', 'flatten_dict', 'to_multidict', 'recursive_update',
    'extract_value_from_dict', 'filter_dict', 'hash_dict', 'strip_dict_fields',
    'fill_template', 'unflatten_dict', 'set_value_to_dict',

    # counters operation
    'counter_min', 'counter_max',

    'terminate_thread', 'async_run_in_thread',

]

SECRETS = {'secret', 'pass'}  #: секретные слова

_secrets = tuple(s.lower() for s in SECRETS)
_secrets_replacer = '*****'


def random_string(length: int):
    """Generates a pseudo-random string of arbitrary length.

    >>> s = random_string(8)

    """

    return ''.join(random.choices(string.ascii_letters, k=length))


async def repeat(
        afunc, retries: int, *args, retry_timeout=1,
        exception_classes=(ConnectionError, TimeoutError),
        logger=None, **kws):
    """Repeats an asynchronous connection (or any operation which requires a
    connection) multiple times if any connection error occurs.

    >>> import asyncio
    >>> async def func(x, y):
    ...     await asyncio.sleep(0.001)
    ...     return x * y
    >>> loop = asyncio.get_event_loop()
    >>> loop.run_until_complete(repeat(func, 3, x=7, y=6))
    42

    :param afunc: an asynchronous function
    :param retries: max number of retries (-1 for infinite)
    :param retry_timeout: timeout in sec between tries
    :param exception_classes: valid exception classes for retrying
    :param args: function arguments
    :param kws: function keywords
    :param logger: optional logger class

    :raises StopIteration: sometimes if it can't catch the last exception (for
        example if zero repeats was provided)
    """

    exc = None

    if retries == -1:
        retries = float('Inf')

    while retries:
        try:
            result = await afunc(*args, **kws)
        except exception_classes as err:
            if logger:
                logger.info('Connection failed due to %s. Retrying...', err.__class__.__name__)
            exc = err
            await asyncio.sleep(retry_timeout)
            retries -= 1
            continue
        else:
            return result
    else:
        if exc:
            raise exc
        else:
            raise StopIteration


def wrap_secrets(data: dict) -> dict:
    """Wraps secrets in asterisks.

    >>> wrap_secrets({'password': 'qwerte1', 'name': 'dogs'})
    {'password': '*****', 'name': 'dogs'}

    """

    for key, value in data.items():
        if isinstance(value, dict):
            data[key] = wrap_secrets(value)
        elif isinstance(key, str):
            if any(s in key.lower() for s in _secrets):
                data[key] = _secrets_replacer
    return data


def register(where: dict):
    """A decorator function which registers a function in a mapping. Function
    name will be converted to lowercase. All trailing '_' symbols will be
    removed (this is essential if one requires to name a function the same way
    as some of the default python functions (for example: "in_" will be
    converted to "in" in the mapping.

    >>> functions = {}
    >>> @register(functions)
    ... def int_(x):
    ...     return int(x)
    >>> functions['int']('1')
    1

    :param where: mapping
    """

    def decorator(f):

        name = f.__name__.lower().rstrip('_')
        if name in where:
            raise ValueError('Two methods with the same name %s.' % name)

        if isinstance(f, FunctionType):
            where[name] = f
        elif abc.ABC not in f.__bases__:
            where[name] = f
            f.name = name

        def wrap(*args, **kws):
            return f(*args, **kws)

        return wrap

    return decorator


def strip_dict_fields(obj: dict, prefix: str = '_') -> dict:
    """Removes fields starting with prefix and creates a new dict object.

    >>> strip_dict_fields({'_id': 43, 'name': 'shites'})
    {'name': 'shites'}

    """

    if isinstance(obj, dict):
        new = {}
        for key, value in obj.items():
            if not key.startswith(prefix):
                new[key] = strip_dict_fields(value, prefix=prefix)
        return new
    elif isinstance(obj, Iterable) and not isinstance(obj, str):
        return [strip_dict_fields(sub, prefix=prefix) for sub in obj]
    else:
        return obj


def hash_dict(obj: dict, meta_prefix: str = None) -> int:
    """Returns an md5 hash of a dictionary object.

    >>> d1 = {'name': 'Dick', 'surname': 'Shitman'}
    >>> d2 = {'name': 'Dick', 'surname': 'Shitman', '_meta': {'a': 1}, '_hash': 42}
    >>> hash_dict(d1) == hash_dict(d2, meta_prefix='_')
    True

    :param obj: a dict
    :param meta_prefix: a prefix for metadata (i.e. fields with such prefix
        will be ignored)
    """

    if meta_prefix:
        obj = strip_dict_fields(obj, prefix=meta_prefix)
    obj = dumps(
        obj, uuid_mode=UM_CANONICAL, ensure_ascii=True, datetime_mode=DM_ISO8601,
        number_mode=NM_DECIMAL, allow_nan=False, default=dict, sort_keys=True)
    h = md5(obj.encode()).hexdigest()
    return int(h, 16)


def unflatten_dict(data: dict, delimiter='.') -> dict:
    """
    >>> unflatten_dict({'a.b.c': True, 'a.c.d': False, 'e': True})
    {'a': {'b': {'c': True}, 'c': {'d': False}}, 'e': True}

    """

    _data = {}

    for key, value in data.items():
        key = key.split(delimiter)
        _d = _data
        for k in key[:-1]:
            if k in _d:
                _d = _d[k]
            else:
                _new_d = {}
                _d[k] = _new_d
                _d = _new_d
        _d[key[-1]] = value

    return _data


def flatten_dict(data: dict, delimiter='.') -> dict:
    """Converts multi-layer dict into a flat dict.

    >>> flatten_dict({'many': {'dogs': {'how_many': 41}}, 'name': 'dogs'}, delimiter='_')
    {'many_dogs_how_many': 41, 'name': 'dogs'}

    >>> flatten_dict({'come': [{'get': {'some': 'shit'}}]})
    {'come': [{'get.some': 'shit'}]}

    :param data: a dict to convert
    :param delimiter: defines how keys will be delimited in a flat structure
        default is dot
    :returns: a new flattened dict
    """

    if isinstance(data, dict):
        _data = {}
        for key, value in data.items():
            prefix = f'{key}{delimiter}'
            if isinstance(value, dict):
                value = flatten_dict(value, delimiter=delimiter)
                for k, v in value.items():
                    k = f'{prefix}{k}'
                    _data[k] = v
            elif isinstance(value, (list, tuple)):
                _data[key] = [flatten_dict(obj, delimiter=delimiter) for obj in value]
            else:
                _data[key] = value
        return _data
    else:
        return data


def to_multidict(kwargs: dict):
    md = multidict.MultiDict()
    for k, v in kwargs.items():
        if isinstance(v, list):
            for _v in v:
                md.add(k, _v)
        else:
            md.add(k, v)
    return md


def recursive_update(obj1: dict, obj2: dict) -> dict:
    """Recursively updates a mapping from another mapping.

    >>> recursive_update({'client': {'name': 'Doe', 'surname': 'John'}}, {'client': {'name': 'Sobaken', 'value': 100}})
    {'client': {'name': 'Sobaken', 'surname': 'John', 'value': 100}}
    """

    if isinstance(obj1, dict):
        if isinstance(obj2, dict):
            for key, value in obj2.items():
                if key in obj1:
                    obj1[key] = recursive_update(obj1[key], value)
                else:
                    obj1[key] = value
        else:
            obj1 = obj2
    elif isinstance(obj1, Iterable) and not isinstance(obj1, str):
        result = []
        if isinstance(obj2, Iterable) and not isinstance(obj2, str):
            for o1, o2 in zip_longest(obj1, obj2):
                if o1 is not None:
                    if o2 is not None:
                        result.append(recursive_update(o1, o2))
                    else:
                        result.append(o1)
                else:
                    result.append(o2)
        obj1 = result
    else:
        obj1 = obj2

    return obj1


def set_value_to_dict(obj: dict, key: str, value, delimiter='.'):
    """Sets value to dict.

    >>> obj = {'data': {}}
    >>> set_value_to_dict(obj, 'data.shite.name', True)
    >>> obj
    {'data': {'shite': {'name': True}}}

    :param obj: a dictionary
    :param key: a key
    :param value: value to set
    :param delimiter:
    """

    key = key.split(delimiter)
    for _key in key[:-1]:
        if _key not in obj:
            obj[_key] = {}
        obj = obj[_key]
    obj[key[-1]] = value


def extract_value_from_dict(obj: dict, key: str, default=KeyError, delimiter='.'):
    """Extracts a value from a multi-layer mapping using a list of keys.

    >>> extract_value_from_dict({'duration': {'gt': '2018-01-05'}}, 'duration.gt')
    '2018-01-05'

    :param obj: a dictionary
    :param key: a key
    :param default: default value to return if not found (or default exception
        to raise)
    :param delimiter:
    """

    for key in key.split(delimiter):
        if key:
            if key in obj:
                obj = obj[key]
            else:
                if type(default) is type:
                    if issubclass(default, Exception):
                        raise default(key)
                return default
    return obj


def _filter_field(obj, keys):
    for n, key in enumerate(keys):
        if isinstance(obj, dict):
            return {key: _filter_field(obj.get(key), keys[n+1:])}
        elif isinstance(obj, Iterable) and not isinstance(obj, str):
            return [_filter_field(o, keys[n:]) for o in obj]
    return obj


def filter_dict(obj: Union[dict, list, tuple], fields: Iterable[str], delimiter='.') -> dict:
    """Filters a dict keys. A resulting mapping will contain only values matching
    provided key structure.

    >>> filter_dict({'a': {'b': {'c': 6, 'd': 7}}, 'e': 1}, ['a.b.c', 'e'])
    {'a': {'b': {'c': 6}}, 'e': 1}

    >>> filter_dict({'a': {'b': [{'c': 4, 'd': 5}, {'e': 6}]}, 'f': 42}, ['a.b.c'])
    {'a': {'b': [{'c': 4}, {'c': None}]}}

    """

    result = {}
    for field in fields:
        result = recursive_update(result, _filter_field(obj, field.split('.')))
    return result


def _format_default():
    return 'NOT_SET'


def fill_template(template: Any, data: dict, default=KeyError, skip_fields=None) -> Any:
    """Позволяет строить JSON объекты из шаблонов. Поддерживает вложенные
    объекты или списки.

    Обозначения для шаблонов:

        "key": "value"      // const значение для поля
        "key": "{key2}"     // шаблон ожидает поле с ключом key2 в списке значений
                            // для шаблона, которое подставит в значение key
                            // ключи полей не чувствительны к регистру
                            // (тип данных сохраняется)
        "key": "d{now+1h}"  // шаблон для datemath выражения
        "key": "{obj.key2}" // запрос по вложенному полю
        "key": "f{Some {string}.}" // форматируемая строка (с подстановкой полей через `str.format`)

    >>> fill_template(
    ...     template={
    ...         "id": "[client_id]", "tag": "testing",
    ...         "name": "f[Hello, {some_name}!]", "l": ["[a]", "[b]", "[a]"]
    ...     },
    ...     data={
    ...         "client_id": 43,
    ...         "some_name": "shiteman",
    ...         "a": "Sobaki", "b": "Koty", "c": "Unknown"
    ...     })
    {'id': 43, 'tag': 'testing', 'name': 'Hello, shiteman!', 'l': ['Sobaki', 'Koty', 'Sobaki']}

    :param template: шаблон, как правило это строка dict или list
    :param data: мэппинг с данными, допускаются вложенные словари
    :param default: значение по-умолчанию для поля, если в мэппинге его не найдено
    :param skip_fields: какие поля следует пропускать при анализе шаблона
       (например, если в шаблоне есть вложенный шаблон, который не надо парсить)
    """

    if isinstance(template, dict):
        t = {}
        if skip_fields:
            for key, value in template.items():
                if key in skip_fields:
                    t[key] = value
                else:
                    t[key] = fill_template(value, data, default=default, skip_fields=skip_fields)
        else:
            for key, value in template.items():
                t[key] = fill_template(value, data, default=default, skip_fields=skip_fields)
        template = t

    elif isinstance(template, str):

        if template.endswith(']'):

            # парсер для подстановки значения
            if template.startswith('['):
                key = template.strip('[]')
                template = extract_value_from_dict(data, key, default=default)

            # парсер datemath объекта
            elif template.startswith('d['):
                template = str(dm(template[1:].strip('[]')).date())

            # парсер format string объекта
            elif template.startswith('f['):
                template = template[1:].strip('[]').format_map(defaultdict(_format_default, data))

    elif isinstance(template, Iterable):
        template = [
            fill_template(value, data, default=default, skip_fields=skip_fields)
            for value in template
        ]

    return template


def terminate_thread(thread):
    """Terminates a python thread from another thread.

    Found it on stack overflow:
    http://code.activestate.com/recipes/496960-thread2-killable-threads/

    as an only real way to stop a stuck python thread.

    :param thread: a threading.Thread instance
    """

    if not thread.isAlive():
        return

    exc = ctypes.py_object(SystemExit)
    res = ctypes.pythonapi.PyThreadState_SetAsyncExc(
        ctypes.c_long(thread.ident), exc)
    if res == 0:
        raise ValueError("nonexistent thread id")
    elif res > 1:
        # """if it returns a number greater than one, you're in trouble,
        # and you should call it again with exc=NULL to revert the effect"""
        ctypes.pythonapi.PyThreadState_SetAsyncExc(thread.ident, None)
        raise SystemError("PyThreadState_SetAsyncExc failed")


async def async_run_in_thread(f, *args, max_exec_time: float = 10.0, **kws):
    """Run an async function in a separate thread.

    :param f: function
    :param args: function arguments
    :param kws: function arguments
    :param max_exec_time: max execution time in seconds
    :return: function result
    """

    loop = asyncio.get_event_loop()
    f = partial(f, *args, **kws)

    with ThreadPoolExecutor(max_workers=1) as tp:
        future = loop.run_in_executor(tp, f)
        try:
            result = await asyncio.wait_for(future, max_exec_time, loop=loop)
        except ConcurrentTimeoutError:
            tp.shutdown(wait=False)
            for t in tp._threads:
                terminate_thread(t)
            raise
        else:
            return result


def counter_min(c1: Counter, c2: Counter) -> Counter:
    """Returns a counter containing a minimum of two counters.

    >>> c = counter_min(Counter({'a': 5, 'b': 10}), Counter({'a': 2}))
    >>> sorted(list(c.items()))
    [('a', 2)]

    """

    new_counter = Counter()

    if not c1 or not c2:
        return new_counter

    for key in set(chain(c1.keys(), c2.keys())):
        n = min(c1[key], c2[key])
        if n:
            new_counter[key] = n
    return new_counter


def counter_max(c1: Counter, c2: Counter) -> Counter:
    """Returns a counter containing a maximum of two counters.

    >>> c = counter_max(Counter({'a': 2, 'b': 10}), Counter({'a': 5}))
    >>> sorted(list(c.items()))
    [('a', 5), ('b', 10)]

    """

    if not c1:
        return c2
    elif not c2:
        return c1

    new_counter = Counter()
    for key in set(chain(c1.keys(), c2.keys())):
        n = max(c1[key], c2[key])
        if n:
            new_counter[key] = n
    return new_counter
